import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '../translate.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  demandeAccueilForm: FormGroup;
  constructor(private router: Router,
              private fb: FormBuilder,
              private translateService: TranslateService) { }

  ngOnInit() {
    localStorage.setItem('lang', 'fr');

    this.demandeAccueilForm = new FormGroup({
      nom: new FormControl('', Validators.required),
      prenom: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
    });

  }

  selectLanguage(str) {
    const test = this.select1(str);
    localStorage.setItem('lang', str);
    return test;
  }

  select1(str) {
    this.translateService.selectLanguage(str);
  }

  editFormDemande() {
    this.router.navigate(['/invite/demande']);
    localStorage.setItem('FormAccueil', 'true');
    localStorage.setItem('FormAccueilNom', this.demandeAccueilForm.value['nom']);
    localStorage.setItem('FormAccueilPrenom', this.demandeAccueilForm.value['prenom']);
    localStorage.setItem('FormAccueilEmail', this.demandeAccueilForm.value['email']);
  }

}
