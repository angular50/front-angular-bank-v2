import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListeDemandesComponent } from './liste-demandes/liste-demandes.component';
import { AdminComponent } from './admin/admin.component';
import { Routes, RouterModule } from '@angular/router';
import { MenuDroitAdminComponent } from './menu-droit-admin/menu-droit-admin.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ListeAgentsComponent } from './agents/liste-agents/liste-agents.component';
import { DetailAgentComponent } from './agents/detail-agent/detail-agent.component';
import { AjoutAgentComponent } from './agents/ajout-agent/ajout-agent.component';
import {NgxPaginationModule} from 'ngx-pagination';



const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: 'liste-demandes', component: ListeDemandesComponent },
      { path: 'liste-agents', component: ListeAgentsComponent },
      { path: 'detail-agent/:id', component: DetailAgentComponent },
      { path: 'ajout-agent', component: AjoutAgentComponent },
      { path: 'login', component: LoginComponent },
      { path: '',   redirectTo: 'admin', pathMatch: 'full' }
    ],
  }
];


@NgModule({
  declarations: [
    ListeDemandesComponent,
    AdminComponent,
    MenuDroitAdminComponent,
    LoginComponent,
    ListeAgentsComponent,
    DetailAgentComponent,
    AjoutAgentComponent
  ],
  imports: [
    RouterModule.forChild(adminRoutes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  bootstrap: [AdminComponent]
})
export class AdminModule { }
