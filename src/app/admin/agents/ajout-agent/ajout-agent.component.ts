import { TypeTransactionHttpService } from './../../../services/typeTransaction/type-transaction-http.service';
import { TypeTransaction } from './../../../models/typeTransaction/type-transaction';
import { AdminHttpService } from './../../../services/admin/admin-http.service';
import { Admin } from './../../../models/admin/admin';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Agent } from 'src/app/models/agent/agent';
import { AgentHttpService } from 'src/app/services/agent/agent-http.service';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajout-agent',
  templateUrl: './ajout-agent.component.html',
  styleUrls: ['./ajout-agent.component.scss']
})
export class AjoutAgentComponent implements OnInit {
  agent: Agent = null;
  agentForm: FormGroup;
  typeTransactionForm: FormGroup;
  typeTransaction: TypeTransaction;
  utilisateur: Utilisateur;
  admin: Admin;
  constructor(private agentService: AgentHttpService,
              private clientService: ClientHttpService,
              private utilisateurService: UtilisateurHttpService,
              private adminService: AdminHttpService,
              private router: Router,
              private typeTransactionService: TypeTransactionHttpService) { }

  ngOnInit() {
    this.agentForm = new FormGroup({
      nom: new FormControl('', Validators.required),
      prenom: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required),
      pseudo: new FormControl('', Validators.required),
      mdp: new FormControl('', Validators.required),
      matricule: new FormControl('', Validators.required),
    });

    this.typeTransactionForm = new FormGroup({
      type: new FormControl('', Validators.required),
    });
  }

  onSubmit() {
    this.utilisateur = new Utilisateur(
      null,
      this.agentForm.controls['nom'].value,
      this.agentForm.controls['prenom'].value,
      this.agentForm.controls['email'].value,
      this.agentForm.controls['adresse'].value,
      this.agentForm.controls['telephone'].value,
      this.agentForm.controls['pseudo'].value,
      this.agentForm.controls['mdp'].value
    );
    this.utilisateurService.saveUtilisateur(this.utilisateur).subscribe(
      (utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;

        this.adminService.getAdminByIdUtilisateur(1).subscribe(
          (admin: Admin) => {
            this.admin = admin;

            this.agent = new Agent(
              null,
              this.agentForm.controls['matricule'].value,
              this.utilisateur,
              this.admin
            );

            this.agentService.saveAgent(this.agent).subscribe(
              (agent: Agent) => {
                this.agent = agent;
                console.log(agent); },
              err => {console.warn(err); }
            );


          },
            err => {console.warn(err); }
        );

        console.log(utilisateur); },
      err => {console.warn(err); }
    );
    this.router.navigate(['/admin/liste-agents']);

    }


  onSubmitType() {
    this.typeTransaction = new TypeTransaction(
      null,
      this.typeTransactionForm.value.type,
    );
    this.typeTransactionService.saveType(this.typeTransaction).subscribe(
      (typeTransaction: TypeTransaction) => {
        this.typeTransaction = typeTransaction;
        console.log(typeTransaction); },
      err => {console.warn(err); }
    );
    this.router.navigate(['/admin/liste-agents']);

    }

    annuler() {
      this.router.navigate(['/admin/liste-agents']);
    }

}
