import { UtilisateurHttpService } from './../../../services/utilisateur/utilisateur-http.service';
import { Utilisateur } from './../../../models/utilisateur/utilisateur';
import { AgentHttpService } from 'src/app/services/agent/agent-http.service';
import { Component, OnInit } from '@angular/core';
import { Agent } from 'src/app/models/agent/agent';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-detail-agent',
  templateUrl: './detail-agent.component.html',
  styleUrls: ['./detail-agent.component.scss']
})
export class DetailAgentComponent implements OnInit {
  private sub: any;
  agent: Agent = null;
  idAgent: number;
  agentForm: FormGroup;
  utilisateur: Utilisateur;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private agentService: AgentHttpService,
              private utilisateurService: UtilisateurHttpService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idAgent = params["id"];
    });

    this.getAgentById();

    this.agentForm = new FormGroup({
      nom: new FormControl('', Validators.required),
      prenom: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required),
      pseudo: new FormControl('', Validators.required),
      mdp: new FormControl('', Validators.required),
      matricule: new FormControl('', Validators.required),
    });

    if (this.idAgent) {
      this.agentService.getAgentById(this.idAgent).subscribe(
        (agent: Agent) => {
          this.agent = agent;
          this.agentForm.patchValue ({
            nom: this.agent.utilisateur.nom,
            prenom: this.agent.utilisateur.prenom,
            email: this.agent.utilisateur.email,
            adresse: this.agent.utilisateur.adresse,
            telephone: this.agent.utilisateur.telephone,
            pseudo: this.agent.utilisateur.pseudo,
            mdp: this.agent.utilisateur.mdp,
            matricule: this.agent.matricule,
          });
          console.log(agent); },
        err => {console.warn(err); }
      );
    }
  }

  getAgentById() {
    this.agentService.getAgentById(this.idAgent).subscribe(
      (agent: Agent) => {
        this.agent = agent; },
        err => {console.warn(err); }
    );
  }

  onSubmit() {
    this.utilisateur = new Utilisateur(
      this.agent.utilisateur.idutilisateur,
      this.agentForm.controls['nom'].value,
      this.agentForm.controls['prenom'].value,
      this.agentForm.controls['email'].value,
      this.agentForm.controls['adresse'].value,
      this.agentForm.controls['telephone'].value,
      this.agentForm.controls['pseudo'].value,
      this.agentForm.controls['mdp'].value
    );
    this.utilisateurService.saveUtilisateur(this.utilisateur).subscribe(
      (utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        console.log(utilisateur); },
      err => {console.warn(err); }
    );
    }

  annuler() {
    this.router.navigate(['/admin/liste-agents']);
  }
}
