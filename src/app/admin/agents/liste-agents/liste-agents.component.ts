import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { Utilisateur } from './../../../models/utilisateur/utilisateur';
import { UtilisateurHttpService } from './../../../services/utilisateur/utilisateur-http.service';
import { ClientHttpService } from './../../../services/client/client-http.service';
import { Component, OnInit } from '@angular/core';
import { Agent } from 'src/app/models/agent/agent';
import { AgentHttpService } from 'src/app/services/agent/agent-http.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-liste-agents',
  templateUrl: './liste-agents.component.html',
  styleUrls: ['./liste-agents.component.scss']
})
export class ListeAgentsComponent implements OnInit {
  agents: Agent[];
  nb: number;
  agent: Agent;
  utilisateur: Utilisateur;
  recherMatriculeAgentForm: FormGroup;
  recherNomAgentForm: FormGroup;
  utilisateurs: Utilisateur[];
  agentMatricule: Agent;
  utilisateurRecherche: Utilisateur[];
  agentsNom: Agent[];
  p: number = 1;
  constructor(private agentService: AgentHttpService,
              private clientService: ClientHttpService,
              private utilisateurService: UtilisateurHttpService,
              private compteService: CompteHttpService,
              private router: Router) { }

  ngOnInit() {
    this.recherMatriculeAgentForm = new FormGroup({
      matricule: new FormControl('', Validators.required),
    });
    this.recherNomAgentForm = new FormGroup({
      nom: new FormControl('', Validators.required),
    });

    this.getAgentsByIdAdmin();
  }

  getAgentsByIdAdmin() {
    this.agentService.getAgentsByIdAdmin(1).subscribe(
      (agents: Agent[]) => {
        this.agents = agents;
        console.log(agents); },
      err => {console.warn(err); }
    );
  }

  editAgent(idAgent: number) {
    this.router.navigate(['/admin/detail-agent/', idAgent]);
  }

  editFormAjoutAgent() {
    this.router.navigate(['/admin/ajout-agent/']);
  }

  deleteUser(idAgent: number, idUtilisateur: number) {
this.clientService.getNbClientsByIdAgent(idAgent).subscribe(
  (nb: number) => {
    this.nb = nb;

    if (this.nb !== 0) {
      console.log('Impossible à supprimer');
      alert('Impossible de supprimer un agent avec des clients !!!!');
    } else {
      console.log('OK');
      this.agentService.deleteAgentById(idAgent).subscribe(
        (agent: Agent) => {
          this.agent = agent;
          this.utilisateurService.deleteUtilisateurById(idUtilisateur).subscribe(
            (utilisateur: Utilisateur) => {
              this.utilisateur = utilisateur;
              console.log(utilisateur); },
            err => {console.warn(err); }
          );
          console.log(agent); },
        err => {console.warn(err); }
      );
    }
    console.log(nb); },
  err => {console.warn(err); }
);
}

onSubmitMatricule() {
  this.agentService.getAgentByMatricule(this.recherMatriculeAgentForm.value.matricule).subscribe(
      (agentMatricule: Agent) => {
        this.agentMatricule = agentMatricule;
        console.log(agentMatricule); },
          err => {console.warn(err); }
        );
}

onSubmitNom() {

  this.utilisateurService.getUtilisateurByName(this.recherNomAgentForm.value.nom).subscribe(
    (utilisateurRecherche: Utilisateur[]) => {
      this.utilisateurRecherche = utilisateurRecherche;
      console.log(utilisateurRecherche); },
        err => {console.warn(err); }
      );


  this.agentService.getAgents().subscribe(
    (agentsNom: Agent[]) => {
      this.agentsNom = agentsNom.filter(rech => rech.utilisateur.nom === this.recherNomAgentForm.value.nom);
      console.log(agentsNom); },
        err => {console.warn(err); }
  );
}

}
