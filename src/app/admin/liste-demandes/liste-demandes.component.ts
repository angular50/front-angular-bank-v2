import { AdminHttpService } from './../../services/admin/admin-http.service';
import { DemandeOuverture } from './../../models/demandeOuverture/demande-ouverture';
import { DemandeOuvertureHttpService } from './../../services/demandeOuverture/demande-ouverture-http.service';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { Agent } from './../../models/agent/agent';
import { AgentHttpService } from './../../services/agent/agent-http.service';
import { ClientPotentiel } from './../../models/clientPotentiel/client-potentiel';
import { Component, OnInit, Input } from '@angular/core';
import { ClientPoService } from 'src/app/services/clientPo/client-po.service';
import { Client } from 'src/app/models/client/client';
import { Admin } from 'src/app/models/admin/admin';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-demandes',
  templateUrl: './liste-demandes.component.html',
  styleUrls: ['./liste-demandes.component.scss']
})
export class ListeDemandesComponent implements OnInit {
  clientsPo: ClientPotentiel[];
  agents: Agent[];
  nbClient: number;
  clientPo: ClientPotentiel;
  admin: Admin;
  idAgentAttribue: number;
  demandeOuverture: DemandeOuverture;
  date: Date = new Date();
  demandeOuvertures: DemandeOuverture[];
  demandeOuvertureAchanger: DemandeOuverture;
  demandeOuverture2: DemandeOuverture;
  p: number = 1;
  idClientPo: number;

constructor(private clientPoService: ClientPoService,
            private adminService: AdminHttpService,
            private agentService: AgentHttpService,
            private clientService: ClientHttpService,
            private demandeService: DemandeOuvertureHttpService,
            private router: Router) { }

ngOnInit() {
    this.getClientsPo();
    this.getAgents();
    this.getAdminById();
    this.getDemandesOuvertures();
  }

getAdminById() {
      // tslint:disable-next-line: radix
      this.adminService.getAdminById(1).subscribe(
        (admin: Admin) => {
          this.admin = admin;
          console.log(admin); },
        err => {console.warn(err); }
      );
        }

getClientsPo() {
    this.clientPoService.getClientsPo().subscribe(
      (clientsPo: ClientPotentiel[]) => {
        this.clientsPo = clientsPo;
        console.log(clientsPo); },
      err => {console.warn(err); }
    );
  }

getAgents() {
    this.agentService.getAgents().subscribe(
      (agents: Agent[]) => {
        this.agents = agents;
        console.log(agents); },
      err => {console.warn(err); }
    );
  }

getNbClientsByIdAgent(idAgent: number) {
    this.clientService.getNbClientsByIdAgent(idAgent).subscribe(
      (nbClient: number) => {
        this.nbClient = nbClient;
        console.log(nbClient);
      },
      err => {console.warn(err); }
    );
  }

attribuerClientPo(idClientPo: number) {
    this.clientPoService.getClientPoById(idClientPo).subscribe(
      (clientPo: ClientPotentiel) => {
        this.clientPo = clientPo;
        console.log(clientPo);

        this.demandeOuverture = new DemandeOuverture (
          null,
          this.date,
          1,
          this.idAgentAttribue,
          this.clientPo,
          this.admin
        );
        this.demandeService.saveDemande(this.demandeOuverture).subscribe(
          (demandeOuverture: DemandeOuverture) => {
            this.demandeOuverture = demandeOuverture;
            console.log(demandeOuverture); },
          err => {console.warn(err); }
        );


      },
        err => {console.warn(err); }
    );

  }

supprimerClientPo(idClientPo: number) {
    this.clientPoService.deleteClientPoById(idClientPo).subscribe(
      (clientPo: ClientPotentiel) => {
        this.clientPo = clientPo;
        console.log(clientPo);
      },
        err => {console.warn(err); }
    );
  }

getDemandesOuvertures() {
    this.demandeService.getDemandes().subscribe(
      (demandeOuvertures: DemandeOuverture[]) => {
        this.demandeOuvertures = demandeOuvertures;
        console.log(demandeOuvertures); },
      err => {console.warn(err); }
    );
  }

changerAttribution(idAgent: number, idDemandeOuverture) {
    this.demandeService.getDemandeById(idDemandeOuverture).subscribe(
      (demandeOuvertureAchanger: DemandeOuverture) => {
        this.demandeOuvertureAchanger = demandeOuvertureAchanger;

        this.demandeOuverture = new DemandeOuverture (
          this.demandeOuvertureAchanger.iddemandeOuverture,
          this.demandeOuvertureAchanger.dateDemande,
          this.demandeOuvertureAchanger.valide,
          idAgent,
          this.demandeOuvertureAchanger.clientpotentiel,
          this.demandeOuvertureAchanger.admin
        );
        this.demandeService.saveDemande(this.demandeOuverture).subscribe(
          (demandeOuverture2: DemandeOuverture) => {
            this.demandeOuverture2 = demandeOuverture2;
            console.log(demandeOuverture2); },
          err => {console.warn(err); }
        );

        console.log(demandeOuvertureAchanger); },
      err => {console.warn(err); }
    );

  }

  openModal(idClientPo: number) {
    localStorage.setItem('idClientPo', JSON.stringify(idClientPo));
    // tslint:disable-next-line: radix
    this.idClientPo = parseInt(localStorage.getItem('idClientPo'));
  }
}
