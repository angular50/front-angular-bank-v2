import { AdminHttpService } from './../../services/admin/admin-http.service';
import { Admin } from 'src/app/models/admin/admin';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Auth } from 'src/app/models/auth/auth';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { AuthentificationService } from 'src/app/services/authentification/authentification.service';
import { AgentHttpService } from 'src/app/services/agent/agent-http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  adminLogForm: FormGroup;
  auth: Auth;
  utilisateur: Utilisateur;
  loggue: number;
  admin: Admin;

  constructor(private fb: FormBuilder,
              private authService: AuthentificationService,
              private adminService: AdminHttpService,
              private router: Router) {
                this.adminLogForm = fb.group({
                  email: '',
                  password: ''
               });
              }

  ngOnInit() {
    this.loggue = 0;
  }

  login = () => {

    this.auth = new Auth(
      this.adminLogForm.value.email,
      this.adminLogForm.value.password
    );
    this.authService.authentification(this.auth).subscribe(
      (utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        localStorage.setItem('idUtilisateur', JSON.stringify(utilisateur.idutilisateur));

        this.adminService.getAdminByIdUtilisateur(this.utilisateur.idutilisateur).subscribe(
            (admin: Admin) => {
              this.admin = admin;
              localStorage.setItem('idadmin', JSON.stringify(admin.idadmin));
              if (localStorage.length === 0) {
                this.router.navigate(['/admin/login']);
                this.loggue = 0;
              } else {
                this.loggue = 1;
                this.router.navigate(['/admin/liste-demandes']);
              }

            },
              err => {console.warn(err); }
          );


      },
      err => {console.warn(err); });
    }

}
