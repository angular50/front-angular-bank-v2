import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuDroitAdminComponent } from './menu-droit-admin.component';

describe('MenuDroitAdminComponent', () => {
  let component: MenuDroitAdminComponent;
  let fixture: ComponentFixture<MenuDroitAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuDroitAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuDroitAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
