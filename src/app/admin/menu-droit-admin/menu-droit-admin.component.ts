import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-droit-admin',
  templateUrl: './menu-droit-admin.component.html',
  styleUrls: ['./menu-droit-admin.component.scss']
})
export class MenuDroitAdminComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
  }

  LogOut() {
    localStorage.clear();
    this.router.navigate(['/accueil']);
  }

  editListeAgents() {
    this.router.navigate(['/admin/liste-agents']);

  }

  editListeDemandes() {
    this.router.navigate(['/admin/liste-demandes']);

  }

}
