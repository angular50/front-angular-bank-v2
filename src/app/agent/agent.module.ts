import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgentComponent } from './agent/agent.component';
import { Routes, RouterModule } from '@angular/router';
import { ListeDemandesComponent } from './demande/liste-demandes/liste-demandes.component';
import { DetailDemandeOuvertureComponent } from './demande/detail-demande-ouverture/detail-demande-ouverture.component';
import { MenuDroitAgentComponent } from './menu-droit-agent/menu-droit-agent.component';
import { ListeClientsComponent } from './clients/liste-clients/liste-clients.component';
import { DetailClientComponent } from './clients/detail-client/detail-client.component';
import { ListeRequetesComponent } from './requetes/liste-requetes/liste-requetes.component';
import { DetailRequeteComponent } from './requetes/detail-requete/detail-requete.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { AjoutClientComponent } from './clients/ajout-client/ajout-client.component';
import { AjoutCompteComponent } from './comptes/ajout-compte/ajout-compte.component';
import { ListeComptesComponent } from './comptes/liste-comptes/liste-comptes.component';
import { DetailCompteComponent } from './comptes/detail-compte/detail-compte.component';
import { EditCompteComponent } from './comptes/edit-compte/edit-compte.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { PdfViewerModule, PdfViewerComponent } from 'ng2-pdf-viewer';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { PiecesJustifsComponent } from './demande/pieces-justifs/pieces-justifs.component';
import { PiecesJustifsImpotsComponent } from './demande/pieces-justifs-impots/pieces-justifs-impots.component';
const agentRoutes: Routes = [
  {
    path: '',
    component: AgentComponent,
    children: [
      { path: 'liste-demandes/:id', component: ListeDemandesComponent },
      { path: 'detail-demande-ouverture/:id', component: DetailDemandeOuvertureComponent },
      { path: 'pieces-justifs/:id', component: PiecesJustifsComponent },
      { path: 'pieces-justifs-impots/:id', component: PiecesJustifsImpotsComponent },
      { path: 'liste-clients/:id', component: ListeClientsComponent },
      { path: 'ajout-client/:id', component: AjoutClientComponent },
      { path: 'detail-client/:id', component: DetailClientComponent },
      { path: 'liste-requetes/:id', component: ListeRequetesComponent },
      { path: 'detail-requete/:id', component: DetailRequeteComponent },
      { path: 'ajout-compte/:id', component: AjoutCompteComponent },
      { path: 'detail-compte/:id', component: DetailCompteComponent },
      { path: 'liste-comptes/:id', component: ListeComptesComponent },
      { path: 'edit-compte/:id', component: EditCompteComponent },
      { path: 'login', component: LoginComponent },
      { path: '',   redirectTo: 'agent', pathMatch: 'full' }
    ],
  }
];


@NgModule({
  declarations: [
    AgentComponent,
    ListeDemandesComponent,
    DetailDemandeOuvertureComponent,
    MenuDroitAgentComponent,
    ListeClientsComponent,
    DetailClientComponent,
    ListeRequetesComponent,
    DetailRequeteComponent,
    LoginComponent,
    AjoutClientComponent,
    AjoutCompteComponent,
    ListeComptesComponent,
    DetailCompteComponent,
    EditCompteComponent,
    PiecesJustifsComponent,
    PiecesJustifsImpotsComponent
  ],
  imports: [
    RouterModule.forChild(agentRoutes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    PdfViewerModule,
    NgxExtendedPdfViewerModule
    ]
})
export class AgentModule { }
