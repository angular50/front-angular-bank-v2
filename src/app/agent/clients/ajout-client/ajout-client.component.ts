import { AgentHttpService } from './../../../services/agent/agent-http.service';
import { UtilisateurHttpService } from './../../../services/utilisateur/utilisateur-http.service';
import { ClientHttpService } from './../../../services/client/client-http.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientPotentiel } from 'src/app/models/clientPotentiel/client-potentiel';
import { ClientPoService } from 'src/app/services/clientPo/client-po.service';
import { Client } from 'src/app/models/client/client';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Agent } from 'src/app/models/agent/agent';

@Component({
  selector: 'app-ajout-client',
  templateUrl: './ajout-client.component.html',
  styleUrls: ['./ajout-client.component.scss']
})
export class AjoutClientComponent implements OnInit {
  clientForm: FormGroup;
  private sub: any;
  idUtilisateurClient: number;
  clientPo: ClientPotentiel;
  idClientPo: number;
  idAgent: number;
  client: Client;
  agent: Agent;
  utilisateur: Utilisateur;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private utilisateurService: UtilisateurHttpService,
              private clientService: ClientHttpService,
              private clientPoService: ClientPoService,
              private agentService: AgentHttpService) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe((params) => {
      this.idUtilisateurClient = params["id"];
    });

        // tslint:disable-next-line: radix
    this.idClientPo = parseInt(localStorage.getItem('idClientPo'));

            // tslint:disable-next-line: radix
    this.idAgent = parseInt(localStorage.getItem('idAgent'));

    this.clientForm = new FormGroup({
      identifiant: new FormControl('', Validators.required),
      revenus: new FormControl('', Validators.required),
    });

    if (this.idUtilisateurClient) {
      this.clientPoService.getClientPoById(this.idClientPo).subscribe(
        (clientPo: ClientPotentiel) => {
          this.clientPo = clientPo;
          this.clientForm.patchValue ({
            revenus: this.clientPo.revenuMensuel,
          });
          console.log(clientPo); },
        err => {console.warn(err); }
      );
    }

    this.agentService.getAgentById(this.idAgent).subscribe(
      (agent: Agent) => {
        this.agent = agent;
        console.log(agent); },
      err => {console.warn(err); }
    );

    this.utilisateurService.getUtilisateurById(this.idUtilisateurClient).subscribe(
      (utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        console.log(utilisateur); },
      err => {console.warn(err); }
    );

  }

  onSubmit() {
this.client = new Client(
  null,
  this.clientForm.value.identifiant,
  this.clientForm.value.pieces,
  this.clientForm.value.revenus,
  this.agent,
  this.utilisateur,
);
this.clientService.saveClient(this.client).subscribe(
  (client: Client) => {
    this.client = client;
    this.router.navigate(['/agent/ajout-compte', this.client.idclient]);
    console.log(client); },
  err => {console.warn(err); }
);
}
}
