import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/models/client/client';
import { ActivatedRoute, Router } from '@angular/router';
import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { Compte } from 'src/app/models/compte/compte';

@Component({
  selector: 'app-detail-client',
  templateUrl: './detail-client.component.html',
  styleUrls: ['./detail-client.component.scss']
})
export class DetailClientComponent implements OnInit {
  private sub: any;
  client: Client;
  idClient: number;
  comptes: Compte[];
  constructor(private route: ActivatedRoute,
              private clientService: ClientHttpService,
              private compteService: CompteHttpService,
              private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idClient = params["id"];
    });

    this.getClientById();
    this.getComptesByIdClient();

  }

  getClientById() {
    this.clientService.getClientById(this.idClient).subscribe(
      (client: Client) => {
        this.client = client; },
        err => {console.warn(err); }
    );
  }

  getComptesByIdClient() {
    this.compteService.getComptesByIdClient(this.idClient).subscribe(
      (comptes: Compte[]) => {
        this.comptes = comptes; },
      err => {console.warn(err); }
    );
  }

  editModifCompte(idCompte: number) {
    localStorage.setItem('idClient', JSON.stringify(this.client.idclient));
    this.router.navigate(['/agent/edit-compte/' , idCompte]);

  }

  editDetailCompte(idCompte: number) {
    localStorage.setItem('idClient', JSON.stringify(this.client.idclient));
    this.router.navigate(['/agent/detail-compte/' , idCompte]);

  }

  createCompte() {
    this.router.navigate(['/agent/ajout-compte/', this.idClient]);
  }
}
