import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/models/client/client';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-liste-clients',
  templateUrl: './liste-clients.component.html',
  styleUrls: ['./liste-clients.component.scss']
})
export class ListeClientsComponent implements OnInit {
  clients: Client[];
  private sub: any;
  idAgent: number;
  idUtilisateur: number;
  p: number = 1;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private clientService: ClientHttpService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idAgent = params["id"];
    });

    this.getClientsByIdAgent();
  }

  getClientsByIdAgent() {
    this.clientService.getClientsByIdAgent(this.idAgent).subscribe(
      (clients: Client[]) => {
        this.clients = clients; },
        err => {console.warn(err); }
    );
  }

  editDetailClient(idClient: number) {
    this.router.navigate(['/agent/detail-client/', idClient]);
  }

}
