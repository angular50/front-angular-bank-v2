import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Compte } from 'src/app/models/compte/compte';
import { Client } from 'src/app/models/client/client';
import { ActivatedRoute, Router } from '@angular/router';
import { parse } from 'querystring';

@Component({
  selector: 'app-ajout-compte',
  templateUrl: './ajout-compte.component.html',
  styleUrls: ['./ajout-compte.component.scss']
})
export class AjoutCompteComponent implements OnInit {
  private sub: any;
  compteForm: FormGroup;
  compte: Compte;
  idClient: number;
  client: Client;
  idAgent: number;
  content = 'mainContent';
  compteCCSDForm: FormGroup;
  compteCCADForm: FormGroup;
  compteCEForm: FormGroup;
  max: number;
  constructor(private route: ActivatedRoute,
              private clientService: ClientHttpService,
              private compteService: CompteHttpService,
              private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idClient = params["id"];
    });

    // tslint:disable-next-line: radix
    this.idAgent = parseInt(localStorage.getItem('idAgent'));

    this.compteForm = new FormGroup({
      numCompte: new FormControl('', Validators.required),
      rib: new FormControl('', Validators.required),
      decouvert: new FormControl('', Validators.required),
      seuilRemu: new FormControl('', Validators.required),
    });

    this.compteCCSDForm = new FormGroup({
      numCompte: new FormControl('', Validators.required),
      rib: new FormControl('', Validators.required),
    });

    this.compteCCADForm = new FormGroup({
      numCompte: new FormControl('', Validators.required),
      rib: new FormControl('', Validators.required),
      decouvert: new FormControl('', Validators.required),
    });

    this.compteCEForm = new FormGroup({
      numCompte: new FormControl('', Validators.required),
      rib: new FormControl('', Validators.required),
      seuilRemu: new FormControl('', Validators.required),
    });

    this.getClientById();
  }

  getClientById() {
    this.clientService.getClientById(this.idClient).subscribe(
      (client: Client) => {
        this.client = client;
        this.max = (this.client.revenuMensuel * 40) / 100; },
        err => {console.warn(err); }
    );
  }

  onSubmit() {
    this.compte = new Compte(
      null,
      this.compteForm.value.numCompte,
      this.compteForm.value.rib,
      0,
      this.compteForm.value.decouvert,
      0,
      this.compteForm.value.seuilRemu,
      0,
      this.client,
    );

    this.compteService.saveCompte(this.compte).subscribe(
      (compte: Compte) => {
        this.compte = compte;
        this.router.navigate(['/agent/liste-clients', this.idAgent]);
      },
        err => {console.warn(err); }
    );

  }

  onSubmitCCSD() {
    this.compte = new Compte(
      null,
      this.compteCCSDForm.value.numCompte,
      this.compteCCSDForm.value.rib,
      0,
      0,
      0,
      0,
      0,
      this.client,
    );

    this.compteService.saveCompte(this.compte).subscribe(
      (compte: Compte) => {
        this.compte = compte;
        this.router.navigate(['/agent/liste-clients', this.idAgent]);
      },
        err => {console.warn(err); }
    );

    }

    onSubmitCCAD() {
      this.compte = new Compte(
        null,
        this.compteCCADForm.value.numCompte,
        this.compteCCADForm.value.rib,
        0,
        this.compteCCADForm.value.decouvert,
        0,
        0,
        0,
        this.client,
      );

      this.compteService.saveCompte(this.compte).subscribe(
        (compte: Compte) => {
          this.compte = compte;
          this.router.navigate(['/agent/liste-clients', this.idAgent]);
        },
          err => {console.warn(err); }
      );

      }

    onSubmitCE() {
      this.compte = new Compte(
        null,
        this.compteCEForm.value.numCompte,
        this.compteCEForm.value.rib,
        0,
        0,
        0,
        this.compteCEForm.value.seuilRemu,
        0,
        this.client,
      );

      this.compteService.saveCompte(this.compte).subscribe(
        (compte: Compte) => {
          this.compte = compte;
          this.router.navigate(['/agent/liste-clients', this.idAgent]);
        },
          err => {console.warn(err); }
      );

      }

}
