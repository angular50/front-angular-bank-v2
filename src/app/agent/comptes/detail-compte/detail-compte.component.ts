import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TransactionHttpService } from 'src/app/services/transaction/transaction-http.service';
import { Transaction } from 'src/app/models/transaction/transaction';

@Component({
  selector: 'app-detail-compte',
  templateUrl: './detail-compte.component.html',
  styleUrls: ['./detail-compte.component.scss']
})
export class DetailCompteComponent implements OnInit {
  private sub: any;
  idClient: number;
  id: number;
  idAgent: number;
  transactions: Transaction[];
  constructor(private transactionService: TransactionHttpService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    console.log(this.router.url);

    this.sub = this.route.params.subscribe((params) => {
      this.id = params['id'];
    });

        // tslint:disable-next-line: radix
    this.idAgent = parseInt(localStorage.getItem('idAgent'));

    this.getTransactionsByIdCompte();

  }
  getTransactionsByIdCompte() {
    this.transactionService.getTransactionsByIdCompteYear(this.id).subscribe(
      (transactions: Transaction[]) => {
        this.transactions = transactions;
        console.log(transactions); },
      err => {console.warn(err); }
    );
  }

  retourListe() {
    this.router.navigate(['/agent/liste-comptes/', this.idAgent]);
  }

}
