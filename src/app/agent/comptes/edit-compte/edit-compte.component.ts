import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { Compte } from 'src/app/models/compte/compte';
import { Client } from 'src/app/models/client/client';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-edit-compte',
  templateUrl: './edit-compte.component.html',
  styleUrls: ['./edit-compte.component.scss']
})
export class EditCompteComponent implements OnInit {
  private sub: any;
  modififCompteForm: FormGroup;
  modififCompteCCSDForm: FormGroup;
  modififCompteCCADForm: FormGroup;
  modififCompteCCEForm: FormGroup;

  idCompte: number;
  idAgent: number;
  compte: Compte;
  client: Client;
  idClient: number;
  typeCompteCCSD: boolean;
  typeCompteCCAD: boolean;
  typeCompteCCE: boolean;
  max: number;

  constructor(private route: ActivatedRoute,
              private clientService: ClientHttpService,
              private compteService: CompteHttpService,
              private router: Router) { }

  ngOnInit() {



    this.sub = this.route.params.subscribe((params) => {
      this.idCompte = params["id"];
    });

        // tslint:disable-next-line: radix
    this.idAgent = parseInt(localStorage.getItem('idAgent'));

    this.modififCompteCCEForm = new FormGroup({
      numCompte: new FormControl('', Validators.required),
      rib: new FormControl('', Validators.required),
      seuilRemu: new FormControl('', Validators.required),
    });


    this.modififCompteCCSDForm = new FormGroup({
      numCompte: new FormControl('', Validators.required),
      rib: new FormControl('', Validators.required),
    });

    this.modififCompteCCADForm = new FormGroup({
      numCompte: new FormControl('', Validators.required),
      rib: new FormControl('', Validators.required),
      decouvert: new FormControl('', Validators.required),
    });

    if (this.idCompte) {
      this.compteService.getCompteById(this.idCompte).subscribe(
        (compte: Compte) => {
          this.compte = compte;

          this.idClient = this.compte.client.idclient;
          this.getClientById();

          this.typeCompteCCSD = this.compte.numCompte.endsWith('A');
          this.typeCompteCCAD = this.compte.numCompte.endsWith('B');
          this.typeCompteCCE = this.compte.numCompte.endsWith('C');


          this.modififCompteCCEForm.patchValue ({
            numCompte: this.compte.numCompte,
            rib: this.compte.rib,
            seuilRemu: this.compte.seuilRemuneration,

          });

          this.modififCompteCCSDForm.patchValue ({
            numCompte: this.compte.numCompte,
            rib: this.compte.rib,

          });

          this.modififCompteCCADForm.patchValue ({
            numCompte: this.compte.numCompte,
            rib: this.compte.rib,
            decouvert: this.compte.decouvert,
          });
          console.log(compte); },
        err => {console.warn(err); }
      );
    }


  }


  getClientById() {
    // a modifier
    this.clientService.getClientById(this.idClient).subscribe(
      (client: Client) => {
        this.client = client;
        this.max = (this.client.revenuMensuel * 40) / 100;
        console.log(client); },
      err => {console.warn(err); }
    );

  }

  onSubmit() {
    this.compte = new Compte(
      this.compte.idcompte,
      this.compte.numCompte,
      this.compte.rib,
      this.compte.solde,
      this.modififCompteForm.value.decouvert,
      this.compte.montantAgios,
      this.modififCompteForm.value.seuilRemu,
      this.compte.montantRemuneration,
      this.client,
    );

    this.compteService.saveCompte(this.compte).subscribe(
      (compte: Compte) => {
        this.compte = compte;
        this.router.navigate(['/agent/detail-client', this.idClient]);
        console.log(compte); },
      err => {console.warn(err); }
    );

  }

  retourListe() {
    this.router.navigate(['/agent/liste-comptes/', this.idAgent]);
  }
}
