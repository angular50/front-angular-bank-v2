import { Component, OnInit } from '@angular/core';
import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { Compte } from 'src/app/models/compte/compte';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-liste-comptes',
  templateUrl: './liste-comptes.component.html',
  styleUrls: ['./liste-comptes.component.scss']
})
export class ListeComptesComponent implements OnInit {
  private sub: any;
  idAgent: number;
  comptes: Compte[];
  recherCompteNum: FormGroup;
  compte: Compte;
  compteAgent: boolean;
  p: number = 1;

  constructor(private compteService: CompteHttpService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.recherCompteNum = new FormGroup({
      numCompte: new FormControl('', Validators.required),
    });

    this.sub = this.route.params.subscribe((params) => {
      this.idAgent = params["id"];
    });

    this.getComptesByIdClient();
  }

  getComptesByIdClient() {
    this.compteService.getComptesByIdAgent(this.idAgent).subscribe(
      (comptes: Compte[]) => {
        this.comptes = comptes; },
      err => {console.warn(err); }
    );
  }

  editDetailCompte(idCompte: number) {
    this.router.navigate(['/agent/detail-compte/', idCompte]);
  }

  editModifCompte(idCompte: number) {
    this.router.navigate(['/agent/edit-compte/', idCompte]);
  }

  onSubmitNum() {

    this.compteService.getCompteByIdNumCompte(this.recherCompteNum.value.numCompte).subscribe(
      (compte: Compte) => {
        this.compte = compte;
        console.log(compte); },
          err => {console.warn(err); }
        );
  }

}
