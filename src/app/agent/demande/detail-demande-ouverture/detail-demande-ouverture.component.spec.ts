import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailDemandeOuvertureComponent } from './detail-demande-ouverture.component';

describe('DetailDemandeOuvertureComponent', () => {
  let component: DetailDemandeOuvertureComponent;
  let fixture: ComponentFixture<DetailDemandeOuvertureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailDemandeOuvertureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailDemandeOuvertureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
