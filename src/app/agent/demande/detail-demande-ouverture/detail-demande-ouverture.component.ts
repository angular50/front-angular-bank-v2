import { AgentHttpService } from 'src/app/services/agent/agent-http.service';
import { UtilisateurHttpService } from 'src/app/services/utilisateur/utilisateur-http.service';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { ClientPoService } from './../../../services/clientPo/client-po.service';
import { DemandeOuverture } from './../../../models/demandeOuverture/demande-ouverture';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DemandeOuvertureHttpService } from 'src/app/services/demandeOuverture/demande-ouverture-http.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClientPotentiel } from 'src/app/models/clientPotentiel/client-potentiel';
import { Client } from 'src/app/models/client/client';
import { Agent } from 'src/app/models/agent/agent';
import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';
import { HttpService } from 'src/app/Shared/http.service';

@Component({
  selector: 'app-detail-demande-ouverture',
  templateUrl: './detail-demande-ouverture.component.html',
  styleUrls: ['./detail-demande-ouverture.component.scss']
})
@Pipe({ name: 'safe' })
export class DetailDemandeOuvertureComponent implements OnInit, PipeTransform {
  private sub: any;
  idDemandeOuv: number;
  demandeOuverture: DemandeOuverture;
  demande: DemandeOuverture;
  demandeOuvForm: FormGroup;
  clientPo: ClientPotentiel;
  idClientPo: number;
  utilisateur: Utilisateur;
  client: Client;
  agent: Agent;
  idAgent: number;
  date: Date = new Date();

  src: any;
  srcImpot: string;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private demandeService: DemandeOuvertureHttpService,
              private clientPoService: ClientPoService,
              private utilisateurService: UtilisateurHttpService,
              private agentService: AgentHttpService,
              public httpService: HttpService,
              private sanitizer: DomSanitizer) { }

  transform(src) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(src);
  }

  ngOnInit() {

    this.sub = this.route.params.subscribe((params) => {
      this.idDemandeOuv = params["id"];
    });

    // tslint:disable-next-line: radix
    this.idClientPo = parseInt(localStorage.getItem('idClientPo'));
    // tslint:disable-next-line: radix
    this.idAgent = parseInt(localStorage.getItem('idAgent'));

    this.getDemandeById();
    this.getAgentById();

    this.demandeOuvForm = new FormGroup({
      nom: new FormControl('', Validators.required),
      prenom: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required),
      revenus: new FormControl('', Validators.required),
      pieces: new FormControl('', Validators.required),
      pseudo: new FormControl('', Validators.required),
      mdp: new FormControl('', Validators.required),
    });

    if (this.idDemandeOuv) {
      this.clientPoService.getClientPoById(this.idClientPo).subscribe(
        (clientPo: ClientPotentiel) => {
          this.clientPo = clientPo;
          this.src = '/assets/clientsPotentiels/' + this.clientPo.idclientpotentiel + '.pdf';
          this.srcImpot = '/assets/clientsPotentiels/Impot/' + this.clientPo.idclientpotentiel + '.pdf';

          this.demandeOuvForm.patchValue ({
            nom: this.clientPo.nom,
            prenom: this.clientPo.prenom,
            email: this.clientPo.email,
            adresse: this.clientPo.adresse,
            telephone: this.clientPo.telephone,
            revenus: this.clientPo.revenuMensuel,

          });
          console.log(clientPo); },
        err => {console.warn(err); }
      );
    }


  }

  printPage() {
    window.print();
  }

  getDemandeById() {
    this.demandeService.getDemandeById(this.idDemandeOuv).subscribe(
      (demandeOuverture: DemandeOuverture) => {
        this.demandeOuverture = demandeOuverture;
        console.log(demandeOuverture); },
        err => {console.warn(err); }
    );
  }

  getAgentById() {
    this.agentService.getAgentById(this.idAgent).subscribe(
      (agent: Agent) => {
        this.agent = agent;
        console.log(agent); },
        err => {console.warn(err); }
    );
  }

  onSubmit() {
    this.utilisateur = new Utilisateur(
      null,
      this.demandeOuvForm.value.nom,
      this.demandeOuvForm.value.prenom,
      this.demandeOuvForm.value.email,
      this.demandeOuvForm.value.adresse,
      this.demandeOuvForm.value.telephone,
      this.demandeOuvForm.value.pseudo,
      this.demandeOuvForm.value.mdp,
    );

    this.utilisateurService.saveUtilisateur(this.utilisateur).subscribe(
        (utilisateur: Utilisateur) => {
          this.utilisateur = utilisateur;
          this.router.navigate(['/agent/ajout-client' , this.utilisateur.idutilisateur]);
          console.log(utilisateur); },
        err => {console.warn(err); }
      );
  }

  majDemandeOuverture(etat: number) {
    this.demandeOuverture = new DemandeOuverture (
      this.demandeOuverture.iddemandeOuverture,
      this.date,
      etat,
      this.idAgent,
      this.clientPo,
      this.agent.admin
    );
    this.demandeService.saveorUpdateDemande(this.demandeOuverture).subscribe(
      (demandeOuverture: DemandeOuverture) => {
        if (etat === 1) {
          this.attente();
        } else {
          this.validation();
        }
        this.demandeOuverture = demandeOuverture;
        console.log(demandeOuverture); },
      err => {console.warn(err); }
    );

  }

  validation() {
    let user = {
      name: this.clientPo.nom,
      email: this.clientPo.email
    };
    this.httpService.sendEmail("http://localhost:3000/sendmailValidationDemande", user).subscribe(
      data => {
        let res:any = data;
        console.log(
          `👏 > 👏 > 👏 > 👏 ${user.name} is successfully register and mail has been sent and the message id is ${res.messageId}`
        );
      },
      err => {console.warn(err); }
    );
  }

  attente() {
    let user = {
      name: this.clientPo.nom,
      email: this.clientPo.email
    };
    this.httpService.sendEmail("http://localhost:3000/sendmailAttenteDemande", user).subscribe(
      data => {
        let res:any = data;
        console.log(
          `👏 > 👏 > 👏 > 👏 ${user.name} is successfully register and mail has been sent and the message id is ${res.messageId}`
        );
      },
      err => {console.warn(err); }
    );
  }

  pdfDomicile() {
    this.router.navigate(['/agent/pieces-justifs/', this.idClientPo]);
  }

  pdfImpot() {
    this.router.navigate(['/agent/pieces-justifs-impots/', this.idClientPo]);

  }
}
