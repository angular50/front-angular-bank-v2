import { Component, OnInit } from '@angular/core';
import { DemandeOuverture } from 'src/app/models/demandeOuverture/demande-ouverture';
import { ActivatedRoute, Router } from '@angular/router';
import { DemandeOuvertureHttpService } from 'src/app/services/demandeOuverture/demande-ouverture-http.service';

@Component({
  selector: 'app-liste-demandes',
  templateUrl: './liste-demandes.component.html',
  styleUrls: ['./liste-demandes.component.scss']
})
export class ListeDemandesComponent implements OnInit {
  private sub: any;
  matriculeAgent: number;
  demandesOuvertures: DemandeOuverture[];
  p: number = 1;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private demandeService: DemandeOuvertureHttpService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.matriculeAgent = params["id"];
    });

    this.getDemandesOuverturesByMatriculeAgent();
  }

  getDemandesOuverturesByMatriculeAgent() {
    this.demandeService.getDemandesByMatriculeAgent(this.matriculeAgent).subscribe(
      (demandesOuvertures: DemandeOuverture[]) => {
        this.demandesOuvertures = demandesOuvertures; },
        err => {console.warn(err); }
    );
  }

  editDetailDemande(idDemande: number, idClientPo: number) {
    localStorage.setItem('idClientPo', JSON.stringify(idClientPo));
    localStorage.setItem('idDemande', JSON.stringify(idDemande));
    this.router.navigate(['/agent/detail-demande-ouverture/', idDemande]);

  }

}
