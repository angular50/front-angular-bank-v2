import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiecesJustifsImpotsComponent } from './pieces-justifs-impots.component';

describe('PiecesJustifsImpotsComponent', () => {
  let component: PiecesJustifsImpotsComponent;
  let fixture: ComponentFixture<PiecesJustifsImpotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiecesJustifsImpotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiecesJustifsImpotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
