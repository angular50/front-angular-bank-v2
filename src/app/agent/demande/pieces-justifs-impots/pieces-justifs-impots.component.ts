import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pieces-justifs-impots',
  templateUrl: './pieces-justifs-impots.component.html',
  styleUrls: ['./pieces-justifs-impots.component.scss']
})
export class PiecesJustifsImpotsComponent implements OnInit {
  src: any;
  idAgent: number;
  id: number;
  private sub: any;

  constructor(private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe((params) => {
      this.id = params["id"];
    });

    this.src = 'http://127.0.0.1:4300/Impot/' + this.id + '.pdf';

            // tslint:disable-next-line: radix
    this.idAgent = (parseInt(localStorage.getItem('idAgent')));
  }

  retourListe() {
    this.router.navigate(['/agent/detail-demande-ouverture/', this.id]);
  }

}
