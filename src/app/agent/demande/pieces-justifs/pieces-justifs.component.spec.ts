import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiecesJustifsComponent } from './pieces-justifs.component';

describe('PiecesJustifsComponent', () => {
  let component: PiecesJustifsComponent;
  let fixture: ComponentFixture<PiecesJustifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiecesJustifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiecesJustifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
