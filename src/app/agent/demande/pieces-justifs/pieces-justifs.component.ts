import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pieces-justifs',
  templateUrl: './pieces-justifs.component.html',
  styleUrls: ['./pieces-justifs.component.scss']
})
export class PiecesJustifsComponent implements OnInit {
  src: any;
  idAgent: number;
  id: number;
  private sub: any;

  constructor(private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe((params) => {
      this.id = params["id"];
    });

    this.src = 'http://127.0.0.1:4300/' + this.id + '.pdf';

            // tslint:disable-next-line: radix
    this.idAgent = (parseInt(localStorage.getItem('idAgent')));
  }

  retourListe() {
    this.router.navigate(['/agent/detail-demande-ouverture/', this.id]);
  }

}
