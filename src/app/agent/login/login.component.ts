import { AgentHttpService } from './../../services/agent/agent-http.service';
import { AgentComponent } from './../agent/agent.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthentificationService } from 'src/app/services/authentification/authentification.service';
import { Router } from '@angular/router';
import { Auth } from 'src/app/models/auth/auth';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Agent } from 'src/app/models/agent/agent';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  agentLogForm: FormGroup;
  auth: Auth;
  utilisateur: Utilisateur;
  loggue: number;
  agent: Agent;

  constructor(private fb: FormBuilder,
              private authService: AuthentificationService,
              private agentService: AgentHttpService,
              private router: Router) {
                this.agentLogForm = fb.group({
                  email: '',
                  password: ''
               });
              }

  ngOnInit() {
    this.loggue = 0;
  }

  login = () => {

    this.auth = new Auth(
      this.agentLogForm.value.email,
      this.agentLogForm.value.password
    );
    this.authService.authentification(this.auth).subscribe(
      (utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        localStorage.setItem('idUtilisateur', JSON.stringify(utilisateur.idutilisateur));

        this.agentService.getAgentByIdUtilisateur(this.utilisateur.idutilisateur).subscribe(
            (agent: Agent) => {
              this.agent = agent;
              localStorage.setItem('idAgent', JSON.stringify(agent.idagent));
              localStorage.setItem('matriculeAgent', JSON.stringify(agent.matricule));
              if (localStorage.length === 0) {
                this.router.navigate(['/agent/login']);
                this.loggue = 0;
              } else {
                this.loggue = 1;
                this.router.navigate(['/agent/liste-clients', this.agent.idagent]);
              }

            },
              err => {console.warn(err); }
          );


      },
      err => {console.warn(err); });
    }

}
