import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuDroitAgentComponent } from './menu-droit-agent.component';

describe('MenuDroitAgentComponent', () => {
  let component: MenuDroitAgentComponent;
  let fixture: ComponentFixture<MenuDroitAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuDroitAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuDroitAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
