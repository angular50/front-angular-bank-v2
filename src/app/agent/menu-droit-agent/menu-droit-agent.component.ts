import { AgentHttpService } from './../../services/agent/agent-http.service';
import { Component, OnInit } from '@angular/core';
import { Agent } from 'src/app/models/agent/agent';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-droit-agent',
  templateUrl: './menu-droit-agent.component.html',
  styleUrls: ['./menu-droit-agent.component.scss']
})
export class MenuDroitAgentComponent implements OnInit {
  idAgent: number;
  idUtilisateur: number;
  constructor(private agentService: AgentHttpService,
              private router: Router) { }

  ngOnInit() {
        // tslint:disable-next-line: radix
        this.idUtilisateur = parseInt(localStorage.getItem('iUtilisateur'));
        // tslint:disable-next-line: radix
        this.idAgent = parseInt(localStorage.getItem('idAgent'));
  }

  editListeClients() {
    this.router.navigate(['/agent/liste-clients', this.idAgent]);

  }

  editListeDemandes() {
    this.router.navigate(['/agent/liste-demandes', this.idAgent]);

  }

  editListeRequetes() {
    this.router.navigate(['/agent/liste-requetes', this.idAgent]);

  }

  editListeComptes() {
    this.router.navigate(['/agent/liste-comptes', this.idAgent]);
  }

  LogOut() {
    localStorage.clear();
    this.router.navigate(['/accueil']);
  }

}
