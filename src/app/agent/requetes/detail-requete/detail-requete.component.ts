import { Requete } from 'src/app/models/requete/requete';
import { RequeteHttpService } from 'src/app/services/requete/requete-http.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail-requete',
  templateUrl: './detail-requete.component.html',
  styleUrls: ['./detail-requete.component.scss']
})
export class DetailRequeteComponent implements OnInit {
  requeteForm: FormGroup;
  idRequete: number;
  requete: Requete;
  idRequeteLoc: number;
  idAgent: number;
  private sub: any;

  constructor(private route: ActivatedRoute,
              private requeteService: RequeteHttpService,
              private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idRequete = params["id"];
    });

    // tslint:disable-next-line: radix
    this.idRequeteLoc = parseInt(localStorage.getItem('idRequeteLoc'));
    // tslint:disable-next-line: radix
    this.idAgent = parseInt(localStorage.getItem('idAgent'));

    this.requeteForm = new FormGroup({
      message: new FormControl('', Validators.required),
      etat: new FormControl('', Validators.required),
    });

    if (this.idRequete) {
      this.requeteService.getRequeteByIdRequete(this.idRequeteLoc).subscribe(
        (requete: Requete) => {
          this.requete = requete;
          this.requeteForm.patchValue ({
            message: this.requete.message,
            etat: this.requete.etat,
          });
          console.log(requete); },
        err => {console.warn(err); }
      );
    }
  }

  onSubmit() {
    this.requete = new Requete(
      this.requete.idrequete,
      this.requete.message,
      this.requete.date,
      2,
      this.requete.client,
      this.requete.typerequete,
    );
    this.requeteService.saveRequete(this.requete).subscribe(
      (requete: Requete) => {
        this.requete = requete;
        this.router.navigate(['/agent/liste-requetes' , this.idAgent]);
        console.log(requete); },
      err => {console.warn(err); }
    );
  }

}
