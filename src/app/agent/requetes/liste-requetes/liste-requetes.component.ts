import { TypeRequete } from './../../../models/typeRequete/type-requete';
import { ClientHttpService } from './../../../services/client/client-http.service';
import { RequeteHttpService } from 'src/app/services/requete/requete-http.service';
import { Component, OnInit } from '@angular/core';
import { Requete } from 'src/app/models/requete/requete';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from 'src/app/models/client/client';

@Component({
  selector: 'app-liste-requetes',
  templateUrl: './liste-requetes.component.html',
  styleUrls: ['./liste-requetes.component.scss']
})
export class ListeRequetesComponent implements OnInit {
  requetes: Requete[];
  requete: Requete;
  private sub: any;
  idAgent: number;
  client: Client;
  typerequete: TypeRequete;
  p: number = 1;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private requeteService: RequeteHttpService,
              private clientService: ClientHttpService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.idAgent = params["id"];
    });

    this.getRequetesByIdAgent();
  }

  getRequetesByIdAgent() {
    this.requeteService.getRequetesByIdAgent(this.idAgent).subscribe(
      (requetes: Requete[]) => {
        this.requetes = requetes; },
        err => {console.warn(err); }
    );
  }

  editRequete(idRequete: number) {
    localStorage.setItem('idRequeteLoc', JSON.stringify(idRequete));
    this.router.navigate(['/agent/detail-requete/', idRequete]);

  }

}
