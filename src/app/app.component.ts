import { TranslateClientService } from './client/translateClient.service';
import { Component } from '@angular/core';
import { TranslateService } from './translate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AppBank';
  constructor(private translateService: TranslateService,
              private translateClientService: TranslateClientService) { }

  selectLanguage(str) {
    let test = this.select1(str);
    return test;
  }

  select1(str) {
    this.translateClientService.selectLanguage(str);
    this.translateService.selectLanguage(str);
  }

}
