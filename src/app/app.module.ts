import { TranslateClientService } from './client/translateClient.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { HeaderComponent } from './components/header/header.component';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { FooterComponent } from './components/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { StatusConnecteDirective } from './directives/status-connecte.directive';
import { ClickDirective } from './directives/click.directive';
import { TranslateService } from './translate.service';
import { TranslatePipe } from './translate.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { TranslateInviteService } from './invite/translateInvite.service';


const appRoutes: Routes = [
  {
    path: 'accueil',
    component: AccueilComponent},
  {
    path: 'invite',
    loadChildren: './invite/invite.module#InviteModule'},
  {
    path: 'client',
    loadChildren: './client/client.module#ClientModule'},
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'},
    {
      path: 'agent',
      loadChildren: './agent/agent.module#AgentModule'},
  {
    path: '',
    redirectTo: '/accueil',
    pathMatch: 'full'
  },

];
@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    HeaderComponent,
    FooterComponent,
    StatusConnecteDirective,
    ClickDirective,
    TranslatePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
  ],
  providers: [TranslateService, TranslateInviteService, TranslateClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
