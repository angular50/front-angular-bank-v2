import { TranslateClientService } from './translateClient.service';
import { FormRequeteComponent } from './requetes/form-requete/form-requete.component';
import { ListeRequetesComponent } from './requetes/liste-requetes/liste-requetes.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListeComptesComponent } from './comptes/liste-comptes/liste-comptes.component';
import { DetailCompteComponent } from './comptes/detail-compte/detail-compte.component';
import { VirementComponent } from './virement/virement.component';
import { ClientComponent } from './client/client.component';
import { Routes, RouterModule } from '@angular/router';
import { MenuDroitClientComponent } from './menu-droit-client/menu-droit-client.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ListeNotificationsComponent } from './notifications/liste-notifications/liste-notifications.component';
import { CookieService } from 'ngx-cookie-service';
import { DetailNotificationComponent } from './notifications/detail-notification/detail-notification.component';
import { HeaderClientComponent } from './components/header-client/header-client.component';
import { AccueilClientComponent } from './accueil/accueil-client/accueil-client.component';
import { PasswordComponent } from './password/password.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { TranslateClientPipe } from './translateClient.pipe';

const clientRoutes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      { path: 'liste-comptes/:id', component: ListeComptesComponent },
      { path: 'liste-notifications/:id', component: ListeNotificationsComponent },
      { path: 'liste-requetes', component: ListeRequetesComponent },
      { path: 'form-requete/:id', component: FormRequeteComponent },
      { path: 'detail-notification/:id', component: DetailNotificationComponent },
      { path: 'detail-compte/:id', component: DetailCompteComponent },
      { path: 'password', component: PasswordComponent },
      { path: 'virement', component: VirementComponent },
      { path: '',   redirectTo: 'client', pathMatch: 'full' }
    ],
  }
];


@NgModule({
  declarations: [ListeComptesComponent,
    DetailCompteComponent,
    VirementComponent,
    ClientComponent,
    MenuDroitClientComponent,
    ListeNotificationsComponent,
    DetailNotificationComponent,
    ListeRequetesComponent,
    FormRequeteComponent,
    HeaderClientComponent,
    AccueilClientComponent,
    PasswordComponent,
  TranslateClientPipe],
  imports: [
    RouterModule.forChild(clientRoutes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule
    ],
  providers: [CookieService, TranslateClientService],
  bootstrap: [ClientComponent]
})
export class ClientModule { }
