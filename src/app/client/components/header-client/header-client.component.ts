import { Component, OnInit } from '@angular/core';
import { Auth } from 'src/app/models/auth/auth';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Client } from 'src/app/models/client/client';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthentificationService } from 'src/app/services/authentification/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-client',
  templateUrl: './header-client.component.html',
  styleUrls: ['./header-client.component.scss']
})
export class HeaderClientComponent implements OnInit {
  auth: Auth;
  utilisateur: Utilisateur;
  client: Client;
  logForm: FormGroup;
  loggue: number;

  constructor(private fb: FormBuilder,
              private authService: AuthentificationService,
              private router: Router) {
    this.logForm = fb.group({
      email: '',
      password: ''
   });
  }

  ngOnInit() {
    this.loggue = 0;
  }

  clearLocalStorage() {
    localStorage.clear();
    this.router.navigate(['/accueil'])
    .then(() => {
      window.location.reload(); // pas tres beau de faire ca !
    });
  }

  retourAccueil() {
    this.router.navigate(['/accueil']);

  }
  editActualites() {
    this.router.navigate(['/invite/actus']);
  }

  editFormDemande() {
    this.router.navigate(['/invite/demande']);
  }

  editEspaceClient(idUtilisateur: number) {
    this.router.navigate(['/client/liste-comptes/', this.utilisateur.idutilisateur]);
  }
  login = () => {

    this.auth = new Auth(
      this.logForm.value.email,
      this.logForm.value.password
    );
    this.authService.authentification(this.auth).subscribe(
      (utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        localStorage.setItem('idUtilisateur', JSON.stringify(utilisateur.idutilisateur));
        if (localStorage.length === 0) {
          this.router.navigate(['/accueil']);
          this.loggue = 0;
        } else {
          this.loggue = 1;
          this.router.navigate(['/client/liste-comptes', utilisateur.idutilisateur]);
        }
      },
      err => {console.warn(err); });
    }
}

