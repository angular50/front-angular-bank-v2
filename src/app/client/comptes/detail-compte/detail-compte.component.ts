import { Component, OnInit } from '@angular/core';
import { Transaction } from 'src/app/models/transaction/transaction';
import { TransactionHttpService } from 'src/app/services/transaction/transaction-http.service';
import { ActivatedRoute, Router } from '@angular/router';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as jsPDF from 'jspdf';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-detail-compte',
  templateUrl: './detail-compte.component.html',
  styleUrls: ['./detail-compte.component.scss']
})
export class DetailCompteComponent implements OnInit {
  transactions: Transaction[];
  id: number;
  idUtilisateur: number;
  private sub: any;
  str: string = '';
  mois = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  moisForm: FormGroup;
  betweenForm: FormGroup;

  constructor(private fb: FormBuilder,
              private transactionService: TransactionHttpService,
              private router: Router,
              private route: ActivatedRoute) {
                this.moisForm = fb.group({
                  choixMois : new FormControl('', Validators.required),
                });
                this.betweenForm = fb.group({
                  dateDebut : new FormControl('', Validators.required),
                  dateFin : new FormControl('', Validators.required),
                });
               }

  ngOnInit() {

    this.sub = this.route.params.subscribe((params) => {
      this.id = params["id"];
    });

        // tslint:disable-next-line: radix
    this.idUtilisateur = (parseInt(localStorage.getItem('idUtilisateur')));

    this.getTransactionsByIdCompte();

}

onSubmit() {
  this.transactionService.getTransactionsByIdCompteMois(this.id, this.moisForm.value.choixMois).subscribe(
    (transactions: Transaction[]) => {
      this.transactions = transactions;
      console.log(transactions); },
    err => {console.warn(err); }
  );
}

onSubmitBetween() {
  this.transactionService.getTransactionsByIdCompteBetween(
                this.id,
                this.betweenForm.value.dateDebut,
                this.betweenForm.value.dateFin
                ).subscribe(
    (transactions: Transaction[]) => {
      this.transactions = transactions;
      console.log(transactions); },
    err => {console.warn(err); }
  );
}

downloadPdf() {
  // tslint:disable-next-line: prefer-for-of
  for (let i = 0; i < this.transactions.length; i++) {
    this.str =  this.str + ' ' +
                this.transactions[i].date.toString().substring(0, 10) + ' ' +
                this.transactions[i].libelle.toString() + ' ' +
                this.transactions[i].montant.toString() + '                    ' + ' \n';
  }

  const doc = new jsPDF();
  doc.text(this.str, 15, 15);
  doc.save('detailTransactions.pdf');
}

generatePdf(action = 'open') {
  const documentDefinition = this.getDocumentDefinition();
  switch (action) {
    case 'open': pdfMake.createPdf(documentDefinition).open();
                 break;
    case 'print': pdfMake.createPdf(documentDefinition).print();
                  break;
    case 'download':
    pdfMake.createPdf(documentDefinition).download();
    break;
    default: pdfMake.createPdf(documentDefinition).open();
             break;
  }
}
getDocumentDefinition() {

  return {
    content: [
    {
      text: 'TRANSACTIONS',
      bold: true,
      fontSize: 20,
      alignment: 'center',
      margin: [0, 0, 0, 20]
    },
    {
    columns: [
      [{

        text: 'Transactions : ' + this.transactions[0].date + ' '
                                + this.transactions[0].libelle + ' '
                                + this.transactions[0].montant
      },
      {

        text: 'Transactions : ' + this.transactions[1].date + ' '
                                + this.transactions[1].libelle + ' '
                                + this.transactions[1].montant
      }],
     ]
    }],
    styles: {
      name: {
        fontSize: 16,
        bold: true
    }
  }
};
}


getTransactionsByIdCompte() {
  this.transactionService.getTransactionsByIdCompte(this.id).subscribe(
    (transactions: Transaction[]) => {
      this.transactions = transactions;
      console.log(transactions); },
    err => {console.warn(err); }
  );
}

getTransactionsByIdCompteMoisDernier() {
  this.transactionService.getTransactionsByIdCompteMoisDernier(this.id).subscribe(
    (transactions: Transaction[]) => {
      this.transactions = transactions;
      console.log(transactions); },
    err => {console.warn(err); }
  );
}

retourListe() {
  this.router.navigate(['/client/liste-comptes/', this.idUtilisateur]);
}

retourAccueil() {
  this.router.navigate(['/accueil']);
}

}
