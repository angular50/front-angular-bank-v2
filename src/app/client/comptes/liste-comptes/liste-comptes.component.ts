import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/models/client/client';
import { Compte } from 'src/app/models/compte/compte';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { SendService } from 'src/app/services/send.service';

@Component({
  selector: 'app-liste-comptes',
  templateUrl: './liste-comptes.component.html',
  styleUrls: ['./liste-comptes.component.scss']
})
export class ListeComptesComponent implements OnInit {
  idUtilisateur: number;
  idClient: number;
  client: Client;
  comptes: Compte[];
  private sub: any;
  idUtiService: number;
  subscription: Subscription;
  p: number = 1;
  constructor(private clientService: ClientHttpService,
              private compteService: CompteHttpService,
              private router: Router,
              private route: ActivatedRoute,
              private sendService: SendService
              ) {  }

  ngOnInit() {

    this.subscription = this.sendService.getIdUtilisateur().subscribe(idUtiService => {
      if (idUtiService) {
        this.idUtiService = idUtiService;
      } else {
        this.idUtiService = idUtiService;
      }
    });

    this.sub = this.route.params.subscribe((params) => {
      this.idUtilisateur = params["id"];
    });

    this.getClientByIdUtilisateur();

  }

  getClientByIdUtilisateur() {
    this.clientService.getClientByIdUtilisateur(this.idUtilisateur).subscribe(
      (client: Client) => {
        this.client = client;
        this.idClient = client.idclient;
        localStorage.setItem('idClient', JSON.stringify(this.idClient));
        this.getComptesByIdClient();
        console.log(client); },
      err => {console.warn(err); }
    );
  }

  getComptesByIdClient() {
    this.compteService.getComptesByIdClient(this.idClient).subscribe(
      (comptes: Compte[]) => {
        this.comptes = comptes; },
      err => {console.warn(err); }
    );
  }

  retourAccueil() {
    this.router.navigate(['/accueil']);
  }

  editDetailCompte(idCompte: number) {
    this.router.navigate(['/client/detail-compte/', idCompte]);
  }
}
