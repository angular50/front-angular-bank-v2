import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuDroitClientComponent } from './menu-droit-client.component';

describe('MenuDroitClientComponent', () => {
  let component: MenuDroitClientComponent;
  let fixture: ComponentFixture<MenuDroitClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuDroitClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuDroitClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
