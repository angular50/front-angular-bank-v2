import { TranslateService } from './../../translate.service';
import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { Component, OnInit, Input } from '@angular/core';
import { Client } from 'src/app/models/client/client';
import { TypeRequete } from 'src/app/models/typeRequete/type-requete';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { Compte } from 'src/app/models/compte/compte';
import { Router } from '@angular/router';
import { SendService } from 'src/app/services/send.service';
import { Observable } from 'rxjs';
import { TranslateClientService } from '../translateClient.service';

@Component({
  selector: 'app-menu-droit-client',
  templateUrl: './menu-droit-client.component.html',
  styleUrls: ['./menu-droit-client.component.scss']
})
export class MenuDroitClientComponent implements OnInit {
  idUtilisateur: number;
  client: Client;
  comptes: Compte[];
  idUtiService: number;
  constructor(private clientService: ClientHttpService,
              private compteService: CompteHttpService,
              private router: Router,
              private sendService: SendService,
              private translateClientService: TranslateClientService,
              private translateService: TranslateService
  ) { }

  ngOnInit(
  ) {

    this.selectLanguage(localStorage.getItem('lang'));

    // tslint:disable-next-line: radix
    this.idUtilisateur = parseInt(localStorage.getItem('idUtilisateur'));
    this.getClientByIdUtilisateur(this.idUtilisateur);


    // test observable
    const data$ = new Observable(observer => {

      observer.next(this.idUtilisateur);
      observer.complete();
  });

    data$.subscribe({
    next: value => console.log(value),
    error: err => console.error(err),
    complete: () => console.log('DONE!')
});
    // fin test

  }

  selectLanguage(str) {
    let test = this.select1(str);
    return test;
  }

  select1(str) {
    this.translateClientService.selectLanguage(str);
    this.translateService.selectLanguage(str);
  }

  getClientByIdUtilisateur(idUtilisateur: number) {
    this.clientService.getClientByIdUtilisateur(idUtilisateur).subscribe(
      (client: Client) => {
        this.client = client;
        // Envoi de l'idUtilisateur dans le service send
        this.idUtiService = this.idUtilisateur;
        this.sendService.sendIdUtilisateur(this.idUtiService);
        console.log('typeOf depuis MenuDroit pour idUtiService --> ' + typeof(this.idUtiService));
        this.compteService.getComptesByIdClient(client.idclient).subscribe(
          (comptes: Compte[]) => {
            this.comptes = comptes;
          },
          err => { console.warn(err); }
        );
        console.log(client);
      },
      err => { console.warn(err); }
    );
  }






  editRequete(typeRequete: TypeRequete) {
    this.router.navigate(['/client/requete', typeRequete.idtyperequete]);
  }

  editListeNotifications() {
    this.router.navigate(['/client/liste-notifications', this.client.idclient]);
  }
  editListeRequetes() {
    this.router.navigate(['/client/liste-requetes']);
  }
  editListeVirements() {
    this.router.navigate(['/client/virement']);
  }
  editListeComptes() {
    this.router.navigate(['/client/liste-comptes', this.idUtilisateur]);
  }

  editPassword() {
    this.router.navigate(['/client/password']);
  }

}

