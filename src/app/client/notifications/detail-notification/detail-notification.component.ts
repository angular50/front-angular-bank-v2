import { Component, OnInit } from '@angular/core';
import { Notif } from 'src/app/models/notif/notif';
import { NotificationHttpService } from 'src/app/services/notification/notification-http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail-notification',
  templateUrl: './detail-notification.component.html',
  styleUrls: ['./detail-notification.component.scss']
})
export class DetailNotificationComponent implements OnInit {
  notifs: Notif[];
  numCompte: string;
  idClient: number;
  private sub: any;
  constructor(private notificationService: NotificationHttpService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe((params) => {
      this.numCompte = params["id"];
    });

    // tslint:disable-next-line: radix
    this.idClient = (parseInt(localStorage.getItem('idClient')));

    this.getNotificationsByNumCompte();

  }

  getNotificationsByNumCompte() {
    this.notificationService.getNotificationsByNumCompte(this.numCompte.toString()).subscribe(
      (notifs: Notif[]) => {
        this.notifs = notifs; },
      err => {console.warn(err); }
    );
  }

  retourListe() {
    this.router.navigate(['/client/liste-notifications/', this.idClient]);
  }

  retourAccueil() {
    this.router.navigate(['/accueil']);
  }

}
