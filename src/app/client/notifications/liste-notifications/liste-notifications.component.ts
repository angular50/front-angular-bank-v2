import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { Notif } from '../../../models/notif/notif';
import { NotificationHttpService } from '../../../services/notification/notification-http.service';
import { Component, OnInit, Input } from '@angular/core';
import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { Compte } from 'src/app/models/compte/compte';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from 'src/app/models/client/client';

@Component({
  selector: 'app-liste-notifications',
  templateUrl: './liste-notifications.component.html',
  styleUrls: ['./liste-notifications.component.scss']
})
export class ListeNotificationsComponent implements OnInit {
  comptes: Compte[];
  client: Client;
  notifs: Notif[];
  id: number;
  private sub: any;
  p: number = 1;

  constructor(private notificationService: NotificationHttpService,
              private compteService: CompteHttpService,
              private clientService: ClientHttpService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe((params) => {
      this.id = params["id"];
    });
    this.getClientById();
  }

  getClientById() {
    this.clientService.getClientById(this.id).subscribe(
      (client: Client) => {
        this.client = client;
        this.getComptesByIdClient();
        this.getNotificationsByIdClient();
      },
      err => {console.warn(err); }
    );
  }


  getComptesByIdClient() {
    this.compteService.getComptesByIdClient(this.client.idclient).subscribe(
      (comptes: Compte[]) => {
        this.comptes = comptes; },
      err => {console.warn(err); }
    );
  }


  getNotificationsByIdClient() {
    this.notificationService.getNotificationsByIdClient(this.client.idclient).subscribe(
      (notifs: Notif[]) => {
        this.notifs = notifs; },
      err => {console.warn(err); }
    );
  }

  retourAccueil() {
    this.router.navigate(['/accueil']);
  }

  editDetailNotif(numCompte: number) {
    this.router.navigate(['/client/detail-notification/', numCompte]);
  }

}
