import { UtilisateurHttpService } from './../../services/utilisateur/utilisateur-http.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/_helpers/must-match.validator';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { DemandeOuverture } from 'src/app/models/demandeOuverture/demande-ouverture';
import { Router } from '@angular/router';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  idUtilisateur: number;
  utilisateur: Utilisateur;
  constructor(private formBuilder: FormBuilder,
              private utilisateurService: UtilisateurHttpService,
              private router: Router) { }

  ngOnInit() {
      this.registerForm = this.formBuilder.group({
          password: ['', [Validators.required, Validators.minLength(6)]],
          confirmPassword: ['', Validators.required],
      }, {
          validator: MustMatch('password', 'confirmPassword')
      });

      // tslint:disable-next-line: radix
      this.idUtilisateur = parseInt(localStorage.getItem('idUtilisateur'));

      this.utilisateurService.getUtilisateurById(this.idUtilisateur).subscribe(
        (utilisateur: Utilisateur) => {
          this.utilisateur = utilisateur; },
          err => {console.warn(err); }
      );

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

// ici l'envoi du nouveau password

      this.utilisateur = new Utilisateur(
  this.utilisateur.idutilisateur,
  this.utilisateur.nom,
  this.utilisateur.prenom,
  this.utilisateur.email,
  this.utilisateur.adresse,
  this.utilisateur.telephone,
  this.utilisateur.pseudo,
  this.registerForm.value.password,
);

      this.utilisateurService.saveUtilisateur(this.utilisateur).subscribe(
    (utilisateur: Utilisateur) => {
      this.utilisateur = utilisateur;
      this.router.navigate(['/client/liste-comptes', this.idUtilisateur]);
      console.log(utilisateur); },
    err => {console.warn(err); }
  );

  }

  retourAccueil() {
    this.router.navigate(['/accueil']);
  }

}
