import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRequeteComponent } from './form-requete.component';

describe('FormRequeteComponent', () => {
  let component: FormRequeteComponent;
  let fixture: ComponentFixture<FormRequeteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRequeteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRequeteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
