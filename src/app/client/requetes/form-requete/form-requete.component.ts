import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Requete } from 'src/app/models/requete/requete';
import { Client } from 'src/app/models/client/client';
import { TypeRequete } from 'src/app/models/typeRequete/type-requete';
import { RequeteHttpService } from 'src/app/services/requete/requete-http.service';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-form-requete',
  templateUrl: './form-requete.component.html',
  styleUrls: ['./form-requete.component.scss']
})
export class FormRequeteComponent implements OnInit {
  requeteForm: FormGroup;
  requete: Requete;
  client: Client;
  date: Date = new Date();
  typerequete: TypeRequete;
  idRequete: number;
  idUtilisateur: number;
  private sub: any;
  idUtiService: number;
  subscription: Subscription;

  constructor(private fb: FormBuilder,
              private requeteService: RequeteHttpService,
              private clientService: ClientHttpService,
              private route: ActivatedRoute,
              private router: Router) {
    this.requeteForm = fb.group({
      message : new FormControl('', Validators.required),
    });
   }

  ngOnInit() {

    this.sub = this.route.params.subscribe((params) => {
      // tslint:disable-next-line: no-string-literal
      this.idRequete = params['id'];
    });

    // tslint:disable-next-line: radix
    this.idUtilisateur = parseInt(localStorage.getItem('idUtilisateur'));

    this.getClientByIdUtilisateur(this.idUtilisateur);
  }

  getClientByIdUtilisateur(idUtilisateur: number) {
      // tslint:disable-next-line: radix
      this.clientService.getClientByIdUtilisateur(idUtilisateur).subscribe(
        (client: Client) => {
          this.client = client;
          console.log(client); },
        err => {console.warn(err); }
      );
        }

  onSubmit() {
    this.requete = new Requete(
      null,
      this.requeteForm.value.message,
      this.date,
      1,
      this.client,
      (this.typerequete = new TypeRequete(
        this.idRequete,
        null
      ))
    );
    this.requeteService.saveRequete(this.requete).subscribe(
      (requete: Requete) => {
        this.requete = requete;
        console.log(requete); },
      err => {console.warn(err); }
    );
  }

  retourAccueil() {
    this.router.navigate(['/accueil']);
  }

  retourEspaceClient() {
    this.router.navigate(['/client/liste-comptes', this.idUtilisateur]);
  }

}

