import { Component, OnInit } from '@angular/core';
import { TypeRequeteHttpService } from 'src/app/services/typeRequete/type-requete-http.service';
import { TypeRequete } from 'src/app/models/typeRequete/type-requete';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-requetes',
  templateUrl: './liste-requetes.component.html',
  styleUrls: ['./liste-requetes.component.scss']
})
export class ListeRequetesComponent implements OnInit {
  typeRequetes: TypeRequete[];
  constructor(private typeRequeteService: TypeRequeteHttpService,
              private router: Router) { }

  ngOnInit() {
    this.getTypesRequetes();
  }

  getTypesRequetes() {
    this.typeRequeteService.getTypesRequetes().subscribe(
      (typeRequetes: TypeRequete[]) => {
        this.typeRequetes = typeRequetes;
        console.log(typeRequetes);
      },
      err => { console.warn(err); }
    );
  }

  retourAccueil() {
    this.router.navigate(['/accueil']);
  }

  editFormRequete(idtyperequete: number) {
    this.router.navigate(['/client/form-requete/', idtyperequete]);
  }

}
