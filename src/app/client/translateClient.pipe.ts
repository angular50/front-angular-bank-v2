import { Pipe, PipeTransform } from '@angular/core';
import { TranslateClientService } from './translateClient.service';

@Pipe({
  name: 'translateClient',
  pure: false
})
export class TranslateClientPipe implements PipeTransform {

  constructor(private translateService: TranslateClientService) { }

  transform(value: any, args?: any): any {
    return this.translateService.translate(value);
  }
}
