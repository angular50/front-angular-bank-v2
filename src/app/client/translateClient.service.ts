import { Injectable } from '@angular/core';
import { TRANSLATIONS } from './translationsClient';

@Injectable()
export class TranslateClientService {

  private currentLanguage = 'fr';

  constructor() { }

  translate(str) {
    return TRANSLATIONS.filter(entrada => entrada['fr'] === str)[0][this.currentLanguage];
  }

  selectLanguage(language) {
    this.currentLanguage = language;
  }

}
