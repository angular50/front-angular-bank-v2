export const TRANSLATIONS = [

  // Pagination
  {
    fr: 'Precedent',
    en: 'Previous',
    ar: 'السابق'
  },
  {
    fr: 'Suivant',
    en: 'Next',
    ar: 'التالية'
  },

  // Menu Droit Client
  {
    fr: 'Mon conseiller',
    en: 'My advisor',
    ar: 'مستشاري'
  },
  {
    fr: 'Mes comptes',
    en: 'My accounts',
    ar: 'حساباتي'
  },
  {
    fr: 'Virement',
    en: 'Transfert',
    ar: 'نقل'
  },
  {
    fr: 'Requetes',
    en: 'Requests',
    ar: 'الطلبات'
  },
  {
    fr: 'Notifications',
    en: 'Notifications',
    ar: 'إشعارات'
  },
  {
    fr: 'Changer MDP',
    en: 'Change Password',
    ar: 'تغيير كلمة المرور'
  },

  // Liste comptes
  {
    fr: 'Bonjour',
    en: 'Hello',
    ar: 'صباح الخير'
  },
  {
    fr: 'Numero de compte',
    en: 'Account number',
    ar: 'رقم الحساب'
  },
  {
    fr: 'Solde : ',
    en: 'Balance : ',
    ar: 'رصيد'
  },
  {
    fr: 'Voir les transactions',
    en: 'View transactions',
    ar: 'عرض المعاملات'
  },

  // Fil arianne
  {
    fr: 'Accueil',
    en: 'Home',
    ar: 'إستقبال'
  },
  {
    fr: 'Espace client',
    en: 'Customer area',
    ar: 'منطقة العملاء'
  },


  {
    fr: 'Virement entre mes comptes',
    en: 'Intern transfert',
    ar: 'التحويل بين حساباتي'
  },
  {
    fr: 'Virement compte externe',
    en: 'Extern transfert',
    ar: 'تحويل حساب خارجي'
  },
  {
    fr: 'Compte debiteur',
    en: 'Debit account',
    ar: 'حساب مدين'
  },
  {
    fr: 'Compte crediteur',
    en: 'Credit account',
    ar: 'حساب ائتماني'
  },
  {
    fr: 'num compte',
    en: 'Account number',
    ar: 'رقم الحساب'
  },
  {
    fr: 'Montant',
    en: 'Amount',
    ar: 'كمية'
  },
  {
    fr: 'Valider',
    en: 'Validate',
    ar: 'يثبت'
  },
  {
    fr: 'Virement OK',
    en: 'Tranfert OK',
    ar: 'نقل موافق'
  },
  {
    fr: 'Virement Impossible',
    en: 'Transfert impossible',
    ar: 'نقل مستحيل'
  },
  {
    fr: 'Votre virement vient d\'etre fait',
    en: 'Transfert is sending',
    ar: 'لقد تم تحويلك للتو'
  },
  {
    fr: 'Votre virement est impossible',
    en: 'Your transfert is impossible',
    ar: 'نقلك مستحيل'
  },

  // MDP
  {
    fr: 'mot de passe',
    en: 'password',
    ar: 'كلمة السر'
  },
  {
    fr: 'confirmation mot de passe',
    en: 'confirm password',
    ar: 'تأكيد كلمة السر'
  },
  {
    fr: 'mot de passe doit avoir 6 caractères min',
    en: 'password must be at least 6 characters long',
    ar: 'الرقم السري يجب الا يقل عن 6 احرف على الاقل'
  },
  {
    fr: 'confirmation doit correspondre',
    en: 'confirmation must match',
    ar: 'يجب أن يتطابق التأكيد'
  },
  {
    fr: 'Modifier',
    en: 'Modify',
    ar: 'تعديل'
  },

  // Requetes
  {
    fr: 'Mes requetes possibles',
    en: 'My possible requests',
    ar: 'طلباتي المحتملة'
  },
  {
    fr: 'CB',
    en: 'Bank card',
    ar: 'بطاقة بنكية'
  },
  {
    fr: 'Cheque',
    en: 'Check',
    ar: 'التحقق من'
  },
  {
    fr: 'Votre message',
    en: 'Your message',
    ar: 'رسالتك'
  },
  {
    fr: 'Ici votre message',
    en: 'Here your message',
    ar: 'هنا رسالتك'
  },
  {
    fr: 'Requete envoyee',
    en: 'Request send',
    ar: 'تم ارسال الطلب'
  },
  {
    fr: 'Votre requete est bien envoyee elle sera traitee au plus vite par votre conseiller',
    en: 'Your request has been sent. It will be processed as quickly as possible by your advisor.',
    ar: 'تم إرسال طلبك. وسوف يقوم المستشار الخاص بك بمعالجته في أسرع وقت ممكن.'
  },
  {
    fr: 'Notif compte n°',
    en: 'Notif of account\'s n°',
    ar: 'رقم حساب الإخطار'
  },
  {
    fr: 'Retour',
    en: 'Back',
    ar: 'إرجاع'
  },
  {
    fr: 'Transactions',
    en: 'Transactions',
    ar: 'المعاملات'
  },
  {
    fr: 'Notifications',
    en: 'Notifications',
    ar: 'إشعارات'
  },
  {
    fr: 'Compte',
    en: 'Account',
    ar: 'الحساب'
  },

  {
    fr: 'Imprimer',
    en: 'Print',
    ar: 'طباعة'
  },

  {
    fr: '30 derniers jours',
    en: 'Last 30 days',
    ar: 'آخر 30 يومًا'
  },

  // Choix Mois

  {
    fr: 'Choix Mois',
    en: 'Choose Month',
    ar: 'اختر الشهر'
  },

];
