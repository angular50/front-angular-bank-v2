import { TypeTransactionHttpService } from './../../services/typeTransaction/type-transaction-http.service';
import { TransactionHttpService } from 'src/app/services/transaction/transaction-http.service';
import { Transaction } from './../../models/transaction/transaction';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'src/app/models/client/client';
import { Compte } from 'src/app/models/compte/compte';
import { ClientHttpService } from 'src/app/services/client/client-http.service';
import { CompteHttpService } from 'src/app/services/compte/compte-http.service';
import { Notif } from 'src/app/models/notif/notif';
import { NotificationHttpService } from 'src/app/services/notification/notification-http.service';

@Component({
  selector: 'app-virement',
  templateUrl: './virement.component.html',
  styleUrls: ['./virement.component.scss']
})
export class VirementComponent implements OnInit {
  idUtilisateur: number;
  idClient: number;
  client: Client;
  comptes: Compte[];
  comptesCrediter: Compte[];
  compteCredit: Compte;
  compteDebit: Compte;
  virementInterneForm: FormGroup;
  virementExterneForm: FormGroup;
  transactionCredit: Transaction;
  transactionDebit: Transaction;
  date: Date = new Date();
  negative: number;
  notificationCredit: Notif;
  notificationDebit: Notif;
  messageDebit: string;
  messageCredit: string;
  solde: number;
  soldeDebit: number;
  etatOpe = 0;
  constructor(private fb: FormBuilder,
              private clientService: ClientHttpService,
              private compteService: CompteHttpService,
              private transactionService: TransactionHttpService,
              private router: Router,
              private typeTransactionService: TypeTransactionHttpService,
              private notifService: NotificationHttpService
  ) {
    this.virementInterneForm = fb.group({
      compteDebiter : new FormControl('', Validators.required),
      compteCrediter : new FormControl('', Validators.required),
      montant : new FormControl('', Validators.required),
    });
    this.virementExterneForm = fb.group({
      compteDebiter : new FormControl('', Validators.required),
      compteCrediter : new FormControl('', Validators.required),
      montant : new FormControl('', Validators.required),
    });
  }

  ngOnInit() {

    // tslint:disable-next-line: radix
    this.idUtilisateur = parseInt(localStorage.getItem('idUtilisateur'));
    this.getClientByIdUtilisateur(this.idUtilisateur);

  }

  getClientByIdUtilisateur(idUtilisateur: number) {
    this.clientService.getClientByIdUtilisateur(idUtilisateur).subscribe(
      (client: Client) => {
        console.log('client');
        this.client = client;
        this.compteService.getComptesByIdClient(client.idclient).subscribe(
          (comptes: Compte[]) => {
            console.log('listeComptes');
            console.log(comptes);
            this.comptes = comptes;
          },
          err => { console.warn(err); }
        );
        console.log(client);
      },
      err => { console.warn(err); }
    );
  }

  onSubmitVirInterne() {

    // tslint:disable-next-line: no-bitwise
    this.negative = ~this.virementInterneForm.value.montant + 1;
    // tslint:disable-next-line: radix
    this.compteService.getCompteById(parseInt(this.virementInterneForm.value.compteCrediter)).subscribe(
      (compteCredit: Compte) => {
        this.compteCredit = compteCredit;
        this.messageDebit = 'virement compte ' + this.compteCredit.numCompte.toString();

        if (((this.compteCredit.decouvert + this.compteCredit.solde) - this.virementInterneForm.value.montant) <= 0 ) {
          this.etatOpe = 1;
        } else {

        // tslint:disable-next-line: radix
        this.compteService.getCompteById(parseInt(this.virementInterneForm.value.compteDebiter)).subscribe(
          (compteDebit: Compte) => {
            this.compteDebit = compteDebit;
            this.messageCredit = 'virement compte ' + this.compteDebit.numCompte.toString();

            this.transactionCredit = new Transaction(
              null,
              this.virementInterneForm.value.montant,
              'virement credit',
              this.date,
              this.compteCredit,
            );

            this.transactionService.saveTransaction(this.transactionCredit).subscribe(
              (transactionCredit: Transaction) => {
                console.log('transaction');
                this.transactionCredit = transactionCredit;

                this.transactionService.getSoldeByIdCompte(this.compteCredit.idcompte).subscribe(
                  (solde: number) => {
                    this.solde = solde;

                    this.compteCredit = new Compte(
                      this.compteCredit.idcompte,
                      this.compteCredit.numCompte,
                      this.compteCredit.rib,
                      this.solde,
                      this.compteCredit.decouvert,
                      this.compteCredit.montantAgios,
                      this.compteCredit.seuilRemuneration,
                      this.compteCredit.montantRemuneration,
                      this.compteCredit.client
                    );

                    this.compteService.saveCompte(this.compteCredit).subscribe(
                      // tslint:disable-next-line: no-shadowed-variable
                      (compteCredit: Compte) => {
                        this.compteCredit = compteCredit;


                        this.notificationCredit = new Notif(
                          null,
                          this.date,
                          this.messageCredit,
                          this.compteCredit
                        );

                        this.notifService.saveNotification(this.notificationCredit).subscribe(
                          (notifCredit: Notif) => {
                            this.notificationCredit = notifCredit;
                            console.log(notifCredit);
                          },
                          err => { console.warn(err); }
                        );

                        this.transactionDebit = new Transaction(
                          null,
                          this.negative,
                          'virement debit',
                          this.date,
                          this.compteDebit,
                        );

                        this.transactionService.saveTransaction(this.transactionDebit).subscribe(
                          (transactionDebit: Transaction) => {
                            console.log('transaction');
                            this.transactionDebit = transactionDebit;


                            this.transactionService.getSoldeByIdCompte(this.compteDebit.idcompte).subscribe(
                              (soldeDebit: number) => {
                                this.soldeDebit = soldeDebit;

                                this.compteDebit = new Compte(
                                  this.compteDebit.idcompte,
                                  this.compteDebit.numCompte,
                                  this.compteDebit.rib,
                                  this.soldeDebit,
                                  this.compteDebit.decouvert,
                                  this.compteDebit.montantAgios,
                                  this.compteDebit.seuilRemuneration,
                                  this.compteDebit.montantRemuneration,
                                  this.compteDebit.client
                                );

                                this.compteService.saveCompte(this.compteDebit).subscribe(
                                  // tslint:disable-next-line: no-shadowed-variable
                                  (compteDebit: Compte) => {
                                    this.compteDebit = compteDebit;

                                    this.notificationDebit = new Notif(
                                      null,
                                      this.date,
                                      this.messageDebit,
                                      this.compteDebit
                                    );

                                    this.notifService.saveNotification(this.notificationDebit).subscribe(
                                      (notifDebit: Notif) => {
                                        this.notificationDebit = notifDebit;
                                      },
                                      err => { console.warn(err); }
                                    );


                                  },
                                  err => { console.warn(err); }
                                );

                              },
                              err => { console.warn(err); }
                            );


                            console.log(transactionDebit);
                          },
                          err => { console.warn(err); }
                        );


                        console.log(compteCredit);
                      },
                      err => { console.warn(err); }
                    );

                    console.log(solde);
                  },
                  err => { console.warn(err); }
                );


                console.log(transactionCredit);
              },
              err => { console.warn(err); }
            );

            console.log(compteDebit);
          },
          err => { console.warn(err); }
        );

        }
        console.log(compteCredit);
      },
      err => { console.warn(err); }
    );

  }

  onSubmitVirExterne() {
  }

  retourAccueil() {
    this.router.navigate(['/accueil']);
  }

  retourEspaceClient() {
    this.router.navigate(['/client/liste-comptes', this.idUtilisateur]);
  }

}
