import { TranslateClientService } from './../../client/translateClient.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'src/app/translate.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  idClient: string;
  loggue: boolean;

  constructor(private translateService: TranslateService,
              private translateClientService: TranslateClientService) { }


  ngOnInit() {
  }


  selectLanguage(str) {
    const test = this.select1(str);
    localStorage.setItem('lang', str);
    return test;
  }

  select1(str) {
    this.translateClientService.selectLanguage(str);
    this.translateService.selectLanguage(str);
  }

}
