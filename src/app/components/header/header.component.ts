import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Client } from 'src/app/models/client/client';
import { AuthentificationService } from 'src/app/services/authentification/authentification.service';
import { Auth } from 'src/app/models/auth/auth';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  auth: Auth;
  utilisateur: Utilisateur;
  client: Client;
  logForm: FormGroup;
  loggue: number;
  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private authService: AuthentificationService,
              private router: Router) {
    this.logForm = fb.group({
      email: '',
      password: ''
   });
  }

  ngOnInit() {
    this.loggue = 0;
  }


  clearLocalStorage() {
    localStorage.clear();
    this.router.navigate(['/accueil'])
    .then(() => {
      window.location.reload(); // pas tres beau de faire ca !
    });
  }

  retourAccueil() {
    this.router.navigate(['/accueil']);

  }
  editActualites() {
    this.router.navigate(['/invite/actus']);
  }

  editFormDemande() {
    this.router.navigate(['/invite/demande']);
  }

  editConvertisseur() {
    this.router.navigate(['/invite/converter']);
  }

  editEspaceClient(idUtilisateur: number) {
    this.router.navigate(['/client/liste-comptes/', this.utilisateur.idutilisateur]);
  }
  login = () => {

    this.auth = new Auth(
      this.logForm.value.email,
      this.logForm.value.password
    );
    this.authService.authentification(this.auth).subscribe(
      (utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        localStorage.setItem('idUtilisateur', JSON.stringify(utilisateur.idutilisateur));
        if (localStorage.length === 0) {
          this.router.navigate(['/accueil']);
          this.loggue = 0;
        } else {
          this.loggue = 1;
          this.router.navigate(['/client/liste-comptes', utilisateur.idutilisateur]);
        }
      },
      err => {console.warn(err); });
    }

test() {
    this.http.post('http://localhost:3000/send', 1).subscribe();
    }

}

