import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appClick]'
})
export class ClickDirective {
  // tslint:disable-next-line: no-inferrable-types
  @HostBinding('style.cursor') cursor: string = 'pointer';
  constructor() { }

}
