import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appStatusConnecte]'
})
export class StatusConnecteDirective {
  @HostBinding('class.link-is-disabled') status: boolean;
  @Input()
  set appStatusConnecte(value) {
    this.status = value;
  }
  constructor() { }

}
