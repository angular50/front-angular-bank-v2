import { Component, OnInit, ViewChild } from '@angular/core';

import { NgForm } from '@angular/forms';
import { Currency } from 'src/app/models/currency/currency.model';
import { Conversion } from 'src/app/models/currency/conversion.model';
import { ConversionResponse } from 'src/app/models/currency/conversion-response.model';
import { CurrencyService } from 'src/app/services/currency.service';
import { ConverterService } from 'src/app/services/converter.service';
import { Router } from '@angular/router';
import { TranslateInviteService } from '../translateInvite.service';
import { TranslateService } from 'src/app/translate.service';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.scss']
})
export class ConverterComponent implements OnInit {

  currencies: Currency[];
  conversion: Conversion;
  error: boolean;
  conversionResponse: ConversionResponse;

  @ViewChild('conversionForm', { static: true }) conversionForm: NgForm;

  constructor(
    private currencyService: CurrencyService,
    private converterService: ConverterService,
    private router: Router,
    private translateInviteService: TranslateInviteService,
    private translateService: TranslateService) { }

  ngOnInit() {

    this.selectLanguage(localStorage.getItem('lang'));

    this.currencies = this.currencyService.findAll();
    this.init();
  }

  selectLanguage(str) {
    let test = this.select1(str);
    return test;
  }

  select1(str) {
    this.translateInviteService.selectLanguage(str);
    this.translateService.selectLanguage(str);
  }

  init(): void {
    this.conversion = new Conversion('EUR', 'USD', null);
    this.error = false;
  }

  newQuery() {
    this.router.navigate(['/invite/converter']);
  }


  convert(): void {
    if (this.conversionForm.form.valid) {
      this.converterService.convert(this.conversion)
        .subscribe(
          response => this.conversionResponse = response,
          error => this.error = true
        );
    }
  }

  get convertedValue(): string {
    if (this.conversionResponse === undefined) {
      return '0';
    }

    return (this.conversion.value * this.conversionResponse.rates[this.conversion.targetCurrency]).toFixed(2);
  }

  get targetQuotation(): number {
    return this.converterService.targetQuotation(this.conversionResponse, this.conversion);
  }

  get sourceQuotation(): string {
    return this.converterService.sourceQuotation(this.conversionResponse, this.conversion);
  }

  get quotationDate(): string {
    return this.converterService.quotationDate(this.conversionResponse);
  }

}
