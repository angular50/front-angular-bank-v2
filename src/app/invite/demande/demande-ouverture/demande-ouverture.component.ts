import { element } from 'protractor';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ClientPoService } from 'src/app/services/clientPo/client-po.service';
import { Router } from '@angular/router';
import { ClientPotentiel } from 'src/app/models/clientPotentiel/client-potentiel';
import { TranslateInviteService } from '../../translateInvite.service';
import { TranslateService } from 'src/app/translate.service';
import { HttpService } from 'src/app/Shared/http.service';

@Component({
  selector: 'app-demande-ouverture',
  templateUrl: './demande-ouverture.component.html',
  styleUrls: ['./demande-ouverture.component.scss']
})
export class DemandeOuvertureComponent implements OnInit {
  demandeForm: FormGroup;
  clientPo: ClientPotentiel;
  demandeOuverture: boolean;
  SelectedFile: File = null;

  constructor(private fb: FormBuilder,
              private clientPoService: ClientPoService,
              private router: Router,
              private translateInviteService: TranslateInviteService,
              private translateService: TranslateService,
              private http: HttpClient,
              public httpService: HttpService) {}

  ngOnInit() {

    this.selectLanguage(localStorage.getItem('lang'));

    if (localStorage.getItem('demandeOuverture')) {
      this.demandeOuverture = true;
    } else {
      this.demandeOuverture = false;
    }

    localStorage.getItem('demandeOuverture');

    this.demandeForm = new FormGroup({
      nom: new FormControl('', Validators.required),
      prenom: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required),
      revenu: new FormControl('', Validators.required),
    });

    if (localStorage.getItem('FormAccueil')) {
      this.demandeForm.patchValue ({
        nom: localStorage.getItem('FormAccueilNom'),
        prenom: localStorage.getItem('FormAccueilPrenom'),
        email: localStorage.getItem('FormAccueilEmail'),
      });
}

  }

  onFileSelected(event) {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    this.SelectedFile = <File> event.target.files[0];
  }

  onUpload() {
    const file = new FormData();
    file.append('file', this.SelectedFile, 'test_' + this.SelectedFile.name);
    this.http.post('http://localhost:8080/files', file)
      .subscribe(res => {
        console.log(res);
      });
  }

  selectLanguage(str) {
    let test = this.select1(str);
    return test;
  }

  select1(str) {
    this.translateInviteService.selectLanguage(str);
    this.translateService.selectLanguage(str);
  }

  onSubmit() {

    this.clientPo = new ClientPotentiel(
      null,
      this.demandeForm.value.nom,
      this.demandeForm.value.prenom,
      this.demandeForm.value.email,
      this.demandeForm.value.adresse,
      this.demandeForm.value.telephone,
      this.demandeForm.value.revenu,
      'test',
    );
    this.clientPoService.saveClientPo(this.clientPo).subscribe(
      (clientPo: ClientPotentiel) => {
        this.clientPo = clientPo;
        localStorage.setItem('demandeOuverture', 'true');
        localStorage.setItem('nom', this.demandeForm.value.nom);
        localStorage.setItem('email', this.demandeForm.value.email);
        localStorage.setItem('idClientPo', JSON.stringify(clientPo.idclientpotentiel));
        this.router.navigate(['/invite/piecesJustif']);
        console.log(clientPo); },
      err => {console.warn(err); }
    );
  }

}
