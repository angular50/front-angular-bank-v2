import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiecesJustifComponent } from './pieces-justif.component';

describe('PiecesJustifComponent', () => {
  let component: PiecesJustifComponent;
  let fixture: ComponentFixture<PiecesJustifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiecesJustifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiecesJustifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
