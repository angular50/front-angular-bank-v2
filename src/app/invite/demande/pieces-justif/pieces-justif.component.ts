import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TranslateInviteService } from '../../translateInvite.service';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/Shared/http.service';

@Component({
  selector: 'app-pieces-justif',
  templateUrl: './pieces-justif.component.html',
  styleUrls: ['./pieces-justif.component.scss']
})
export class PiecesJustifComponent implements OnInit {
  piecesJustifForm: FormGroup;
  SelectedFile: File = null;
  idClientPo: number;

  texttest: string;
  parts: string[];
  constructor(private fb: FormBuilder,
              private translateInviteService: TranslateInviteService,
              private router: Router,
              private http: HttpClient,
              public httpService: HttpService) { }

  ngOnInit() {
    localStorage.getItem('demandeOuverture');
    // tslint:disable-next-line: radix
    this.idClientPo = parseInt(localStorage.getItem('idClientPo'));

    this.piecesJustifForm = new FormGroup({
      piecesJustif: new FormControl('', Validators.required),
      impots: new FormControl('', Validators.required),
    });

  }

  onFileSelected(event) {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    this.SelectedFile = <File> event.target.files[0];
    let test = this.SelectedFile.name;
    this.parts = test.split('.', 2);
    this.texttest = this.parts[1];
  }
  onFileSelected2(event) {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    this.SelectedFile = <File> event.target.files[0];
  }

  onUpload() {
    const file = new FormData();
    file.append('file', this.SelectedFile, this.idClientPo + '.' + this.texttest);
    this.http.post('http://localhost:8080/files', file)
      .subscribe(res => {
        console.log(res);
      });
  }

  onUploadImpot() {
    const file = new FormData();
    file.append('file', this.SelectedFile, this.idClientPo + '.' + this.texttest);
    this.http.post('http://localhost:8080/files/impot', file)
      .subscribe(res => {
        console.log(res);
      });
  }

  onSubmit(){
    this.register();
    this.router.navigate(['/accueil']);

  }

  retourHP() {
    this.router.navigate(['/accueil']);
  }

  register() {
    let user = {
      name: localStorage.getItem('nom'),
      email: localStorage.getItem('email')
    }
    this.httpService.sendEmail("http://localhost:3000/sendmail", user).subscribe(
      data => {
        let res:any = data;
        console.log(
          `👏 > 👏 > 👏 > 👏 ${user.name} is successfully register and mail has been sent and the message id is ${res.messageId}`
        );
      },
      err => {console.warn(err); }
    );
  }

}
