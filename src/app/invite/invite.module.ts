import { StepperComponent } from './stepper/stepper/stepper.component';
import { TranslateInviteService } from './translateInvite.service';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ActualitesComponent } from './actualites/actualites.component';
import { ConverterComponent } from './converter/converter.component';

import { Routes, RouterModule } from '@angular/router';
import { InviteComponent } from './invite/invite.component';
import { FormsModule, ReactiveFormsModule, Validators, FormGroup } from '@angular/forms';
import { TranslateInvitePipe } from './translateInvite.pipe';
import { DemandeOuvertureComponent } from './demande/demande-ouverture/demande-ouverture.component';
import { PiecesJustifComponent } from './demande/pieces-justif/pieces-justif.component';

const inviteRoutes: Routes = [
  {
    path: '',
    component: InviteComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'actus', component: ActualitesComponent },
      { path: 'demande', component: DemandeOuvertureComponent },
      { path: 'piecesJustif', component: PiecesJustifComponent },
      { path: 'converter', component: ConverterComponent },
      { path: '',   redirectTo: 'invite', pathMatch: 'full' }
    ],
  }
];


@NgModule({
  declarations: [
    HomeComponent,
    ActualitesComponent,
    DemandeOuvertureComponent,
    InviteComponent,
    ConverterComponent,
    StepperComponent,
    TranslateInvitePipe,
    PiecesJustifComponent],
  imports: [
    RouterModule.forChild(inviteRoutes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [TranslateInviteService],
  bootstrap: [InviteComponent]
})
export class InviteModule { }
