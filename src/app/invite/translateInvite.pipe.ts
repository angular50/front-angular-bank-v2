import { Pipe, PipeTransform } from '@angular/core';
import { TranslateInviteService } from './translateInvite.service';

@Pipe({
  name: 'translateInvite',
  pure: false
})
export class TranslateInvitePipe implements PipeTransform {

  constructor(private translateService: TranslateInviteService) { }

  transform(value: any, args?: any): any {
    return this.translateService.translate(value);
  }
}
