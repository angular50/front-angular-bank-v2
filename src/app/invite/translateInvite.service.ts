import { Injectable } from '@angular/core';
import { TRANSLATIONS } from './translationsInvite';

@Injectable()
export class TranslateInviteService {

  private currentLanguage = 'fr';

  constructor() { }

  translate(str) {
    return TRANSLATIONS.filter(entrada => entrada['fr'] === str)[0][this.currentLanguage];
  }

  selectLanguage(language) {
    this.currentLanguage = language;
  }

}
