export const TRANSLATIONS = [

  // Demande ouverture
  {
    fr: 'Votre demande ouverture est envoyee',
    en: '',
    ar: ''
  },
  {
    fr: 'Demande ouverture envoyee',
    en: '',
    ar: ''
  },
  {
    fr: 'Demande ouverture de compte',
    en: 'Account opening request',
    ar: 'طلب فتح حساب'
  },
  {
    fr: 'Vous avez deja fait une demande d\'ouverture !!!!',
    en: 'You have already made an opening request !!!!',
    ar: 'لقد قمت بالفعل بتقديم طلب افتتاح !!!!'
  },
  {
    fr: 'Nom',
    en: 'Name',
    ar: 'الإ سم'
  },
  {
    fr: 'Prenom',
    en: 'Firstname',
    ar: 'اِسْم مَسِيحِيّ'
  },
  {
    fr: 'Adresse email',
    en: 'Email address',
    ar: 'البريد الالكتروني'
  },
  {
    fr: 'Adresse Mail obligatoire!',
    en: 'Email address required!',
    ar: '(عنوان البريد الإلكتروني: (مطلوب'
  },
  {
    fr: 'Entrer une adresse Mail!',
    en: 'Enter an Email address',
    ar: 'أدخل عنوان بريد!'
  },
  {
    fr: 'Adresse',
    en: 'Address',
    ar: 'عنوان'
  },
  {
    fr: 'Telephone',
    en: 'Phone',
    ar: 'هاتف'
  },
  {
    fr: 'Revenu mensuel',
    en: 'Monthly income',
    ar: 'الدخل الشهري'
  },
  {
    fr: 'Pieces justificatives',
    en: 'Supporting documents',
    ar: 'قسائم'
  },
  {
    fr: 'Impots',
    en: 'test',
    ar: 'قسائم'
  },
  {
    fr: 'Valider',
    en: 'Validate',
    ar: 'يثبت'
  },
// Converter
  {
    fr: 'CONVERTISSEUR DEVISES',
    en: 'CURRENCY CONVERTER',
    ar: 'محول العملات'
  },
  {
    fr: 'Convertir',
    en: 'Convert',
    ar: 'تحويل'
  },
  {
    fr: 'Nouvelle conversion',
    en: 'New convert',
    ar: 'تحويل جديد'
  },

  // Stepper
  {
    fr: 'Informations personnelles',
    en: 'Personal informations',
    ar: 'معلومات شخصية'
  },
  {
    fr: 'Pieces justificatives',
    en: 'Vouchers',
    ar: 'قسائم'
  },
  {
    fr: 'Validation',
    en: 'Validation',
    ar: 'التحقق'
  },

  // Converter

  {
    fr: 'Francs Suisse',
    en: 'Swiss francs',
    ar: 'فرنك سويسري'
  },
  {
    fr: 'Euro',
    en: 'Euro',
    ar: 'اليورو'
  },
  {
    fr: 'Livre Anglaise',
    en: 'English Pound',
    ar: 'الجنيه الإنجليزي'
  },
  {
    fr: 'Roupie Indienne',
    en: 'Indian rupee',
    ar: 'روبية هندية'
  },
  {
    fr: 'Dollar USA',
    en: 'US dollar',
    ar: 'الدولار الأمريكي'
  },

]


