import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Admin } from '../admin/admin';
export class Agent {
  idagent: number;
  matricule: number;
  utilisateur: Utilisateur;
  admin: Admin;

  constructor(idagent: number, matricule: number, utilisateur: Utilisateur, admin: Admin) {
    this.idagent = idagent;
    this.matricule = matricule;
    this.utilisateur = utilisateur;
    this.admin = admin;
  }
}


