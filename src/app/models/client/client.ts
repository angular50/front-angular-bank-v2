import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';
import { Agent } from './../agent/agent';
export class Client {
  idclient: number;
  identifiant: string;
  piecesJustif: string;
  revenuMensuel: number;
  agent: Agent;
  utilisateur: Utilisateur;

  constructor(idclient: number,
              identifiant: string,
              piecesJustif: string,
              revenuMensuel: number,
              agent: Agent,
              utilisateur: Utilisateur) {

    this.idclient = idclient;
    this.identifiant = identifiant;
    this.piecesJustif = piecesJustif;
    this.revenuMensuel = revenuMensuel;
    this.agent = agent;
    this.utilisateur = utilisateur;
  }
}
