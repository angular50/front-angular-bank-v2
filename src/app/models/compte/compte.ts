import { Client } from '../client/client';

export class Compte {
  idcompte: number;
  numCompte: string;
  rib: string;
  solde: number;
  decouvert: number;
  montantAgios: number;
  seuilRemuneration: number;
  montantRemuneration: number;
  client: Client;

  // tslint:disable-next-line: max-line-length
  constructor(idcompte: number, numCompte: string, rib: string, solde: number, decouvert: number, montantAgios: number, seuilRemuneration: number, montantRemuneration: number, client: Client) {
  this.idcompte = idcompte;
  this.numCompte = numCompte;
  this.rib = rib;
  this.solde = solde;
  this.decouvert = decouvert;
  this.montantAgios = montantAgios;
  this.seuilRemuneration = seuilRemuneration;
  this.montantRemuneration = montantRemuneration;
  this.client = client;

  }
}
