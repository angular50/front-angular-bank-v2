import { ClientPotentiel } from './../clientPotentiel/client-potentiel';
import { Admin } from '../admin/admin';
export class DemandeOuverture {
  iddemandeOuverture: number;
  dateDemande: Date;
  valide: number;
  agentMatricule: number;
  clientpotentiel: ClientPotentiel;
  admin: Admin;

  // tslint:disable-next-line: max-line-length
  constructor(iddemandeOuverture: number, dateDemande: Date, valide: number, agentMatricule: number, clientpotentiel: ClientPotentiel, admin: Admin) {
    this.iddemandeOuverture = iddemandeOuverture;
    this.dateDemande = dateDemande;
    this.valide = valide;
    this.agentMatricule = agentMatricule;
    this.clientpotentiel = clientpotentiel;
    this.admin = admin;
  }
}
