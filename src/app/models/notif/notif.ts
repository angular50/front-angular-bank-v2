import { Compte } from '../compte/compte';

export class Notif {
  idnotification: number;
  date: Date;
  message: string;
  compte: Compte;

  constructor(idnotification: number, date: Date, message: string, compte: Compte) {
    this.idnotification = idnotification;
    this.date = date;
    this.message = message;
    this.compte = compte;
  }
}
