import { Client } from '../client/client';
import { TypeRequete } from '../typeRequete/type-requete';

export class Requete {
  idrequete: number;
  message: string;
  etat: number;
  date: Date;
  client: Client;
  typerequete: TypeRequete;


  constructor(idrequete: number, message: string, date: Date, etat: number, client: Client, typerequete: TypeRequete) {
      this.idrequete = idrequete;
      this.message = message;
      this.etat = etat;
      this.date = date;
      this.client = client;
      this.typerequete = typerequete;
  }

}
