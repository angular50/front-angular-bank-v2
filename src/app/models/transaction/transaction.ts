import { Compte } from '../compte/compte';
import { TypeTransaction } from '../typeTransaction/type-transaction';

export class Transaction {
  idtransaction: number;
  montant: number;
  libelle: string;
  date: Date;
  compte: Compte;
  //typeTransaction: TypeTransaction;

  constructor(idtransaction: number,
              montant: number,
              libelle: string,
              date: Date,
              compte: Compte,
              //typeTransaction: TypeTransaction
              ) {
    this.idtransaction = idtransaction;
    this.montant = montant;
    this.libelle = libelle;
    this.date = date;
    this.compte = compte;
    //this.typeTransaction = typeTransaction;
  }
}
