export class TypeRequete {
  idtyperequete: number;
  type: string;

  constructor(idtyperequete: number, type: string) {
    this.idtyperequete = idtyperequete;
    this.type = type;
  }
}
