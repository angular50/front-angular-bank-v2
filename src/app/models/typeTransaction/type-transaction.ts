export class TypeTransaction {
  idtypetransaction: number;
  type: string;

  constructor(idtypetransaction: number, type: string) {
    this.idtypetransaction = idtypetransaction;
    this.type = type;
  }
}
