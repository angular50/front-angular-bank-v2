export class Utilisateur {
  idutilisateur: number;
  nom: string;
  prenom: string;
  email: string;
  adresse: string;
  telephone: string;
  pseudo: string;
  mdp: string;

  // tslint:disable-next-line: max-line-length
  constructor(idutilisateur: number, nom: string, prenom: string, email: string, adresse: string, telephone: string, pseudo: string, mdp: string) {
    this.idutilisateur = idutilisateur;
    this.nom = nom;
    this.prenom = prenom;
    this.email = email;
    this.adresse = adresse;
    this.telephone = telephone;
    this.pseudo = pseudo;
    this.mdp = mdp;
  }
}
