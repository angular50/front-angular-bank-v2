import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminHttpService {
  private url: string = 'https://heroku-gestibankback.herokuapp.com/admins';

  constructor(private http: HttpClient) { }

  getAdminById = (id: number) => {
    return this.http.get(this.url + `/${id}`);
  }

  getAdminByIdUtilisateur = (id: number) => {
    return this.http.get(this.url + `/utilisateur` + `/${id}`);
  }
}
