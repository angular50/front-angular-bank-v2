import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Agent } from 'src/app/models/agent/agent';

@Injectable({
  providedIn: 'root'
})
export class AgentHttpService {
  private url: string = 'http://localhost:8080/agents';

  constructor(private http: HttpClient) { }

  getAgents = () => {
    return this.http.get(this.url);
  }

  getAgentByIdUtilisateur = (id: number) => {
    return this.http.get(this.url + `/utilisateur` + `/${id}`);
  }

  getAgentByMatricule = (matricule: string) => {
    return this.http.get(this.url + `/matricule` + `/${matricule}`);
  }

  getAgentById = (id: number) => {
    return this.http.get(this.url + `/${id}`);
  }

  getAgentsByIdAdmin = (id: number) => {
    return this.http.get(this.url + `/admin` + `/${id}`);
  }

  deleteAgentById = (id: number) => {
    return this.http.delete(this.url + `/${id}`);
  }

  saveAgent = (agent: Agent) => {
    return this.http.post(this.url, agent);
  }

}
