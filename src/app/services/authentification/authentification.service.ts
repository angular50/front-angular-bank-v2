import { Auth } from './../../models/auth/auth';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  private url: string = 'http://localhost:8080/utilisateurs/login';
  constructor(private http: HttpClient) { }

  authentification(auth: Auth){
    return this.http.post(this.url, auth);
  }

}
