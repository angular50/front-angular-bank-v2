import { Client } from 'src/app/models/client/client';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientHttpService {
  private url: string = 'http://localhost:8080/clients';

  constructor(private http: HttpClient) { }

  getClients = () => {
    return this.http.get(this.url);
  }

  getClientById = (id: number) => {
    return this.http.get(this.url + `/${id}`);
  }

  getClientByIdUtilisateur = (id: number) => {
    return this.http.get(this.url + `/utilisateur` + `/${id}`);
  }

  getClientByIdCompte = (id: number) => {
    return this.http.get(this.url + `/compte` + `/${id}`);
  }

  getClientsByIdAgent = (id: number) => {
    return this.http.get(this.url + `/agent` + `/${id}`);
  }

  getNbClientsByIdAgent = (id: number) => {
    return this.http.get(this.url + `/agent/nb` + `/${id}`);
  }

  saveClient = (client: Client) => {
    return this.http.post(this.url, client);
  }
}
