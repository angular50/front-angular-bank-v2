import { TestBed } from '@angular/core/testing';

import { ClientPoService } from './client-po.service';

describe('ClientPoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientPoService = TestBed.get(ClientPoService);
    expect(service).toBeTruthy();
  });
});
