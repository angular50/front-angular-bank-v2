import { ClientPotentiel } from './../../models/clientPotentiel/client-potentiel';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientPoService {
  private url: string = 'http://localhost:8080/clientspotentiels';

  constructor(private http: HttpClient) { }

  getClientsPo = () => {
    return this.http.get(this.url + `/nonTraites`);
  }

  getClientPoById = (id: number) => {
    return this.http.get(this.url + `/${id}`);
  }

  saveClientPo(clientPo: ClientPotentiel){
    return this.http.post(this.url, clientPo);
  }

  deleteClientPoById = (id: number) => {
    return this.http.delete(this.url + `/${id}`);
  }
}
