import { TestBed } from '@angular/core/testing';

import { CompteHttpService } from './compte-http.service';

describe('CompteHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompteHttpService = TestBed.get(CompteHttpService);
    expect(service).toBeTruthy();
  });
});
