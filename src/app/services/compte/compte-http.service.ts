import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Compte } from 'src/app/models/compte/compte';

@Injectable({
  providedIn: 'root'
})
export class CompteHttpService {
  private url: string = 'http://localhost:8080/comptes';

  constructor(private http: HttpClient) { }

  getComptes = () => {
    return this.http.get(this.url);
  }

  getComptesByIdClient = (id: number) => {
    return this.http.get(this.url + `/client` + `/${id}`);
  }

  getCompteByIdNumCompte = (numCompte: string) => {
    return this.http.get(this.url + `/numCompte` + `/${numCompte}`);
  }

  getComptesByIdAgent = (id: number) => {
    return this.http.get(this.url + `/agent` + `/${id}`);
  }

  getCompteById = (id: number) => {
    return this.http.get(this.url + `/${id}`);
  }

  saveCompte = (compte: Compte) => {
    return this.http.post(this.url, compte);
  }
}
