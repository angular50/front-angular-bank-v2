import { Injectable } from '@angular/core';
import { Currency } from '../models/currency/currency.model';


@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  private currencies: Currency[];

  constructor() { }

  // Improvised database
  private currenciesObj = [ // http://fixer.io
    { "initials": "CHF", "description": "Francs Suisse" },
    { "initials": "EUR", "description": "Euro" },
    { "initials": "GBP", "description": "Livre Anglaise" },
    { "initials": "INR", "description": "Roupie Indienne" },
    { "initials": "USD", "description": "Dollar USA" },
    ];

    findAll(): Currency[] {
      if (this.currencies) {
        return this.currencies;
      }

      this.currencies = [];

      for (let currencyObj of this.currenciesObj) {
        let currency: Currency = new Currency();
        Object.assign(currency, currencyObj);
        this.currencies.push(currency);
      }

      return this.currencies;

    }
}
