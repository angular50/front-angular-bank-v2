import { DemandeOuverture } from './../../models/demandeOuverture/demande-ouverture';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DemandeOuvertureHttpService {
  private url: string = 'http://localhost:8080/demandesouvertures';

  constructor(private http: HttpClient) { }

  getDemandes = () => {
    return this.http.get(this.url);
  }

  getDemandeById = (id: number) => {
    return this.http.get(this.url + `/${id}`);
  }

  getDemandesByMatriculeAgent = (matricule: number) => {
    return this.http.get(this.url + `/agent` + `/${matricule}`);
  }
  saveDemande(demande: DemandeOuverture) {
    return this.http.post(this.url, demande);
  }
  saveorUpdateDemande(demande: DemandeOuverture) {
    return this.http.put(this.url, demande);
  }

}
