import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Notif } from 'src/app/models/notif/notif';

@Injectable({
  providedIn: 'root'
})
export class NotificationHttpService {
  private url: string = 'http://localhost:8080/notifications';

  constructor(private http: HttpClient) { }

  getNotifications = () => {
    return this.http.get(this.url);
  }

  getNotificationsByIdCompte = (id: number) => {
    return this.http.get(this.url + `/compte` + `/${id}`);
  }

  getNotificationsByNumCompte = (numCompte: string) => {
    return this.http.get(this.url + `/numCompte` + `/${numCompte}`);
  }

  getNotificationsByIdClient = (id: number) => {
    return this.http.get(this.url + `/client` + `/${id}`);
  }

  saveNotification(notification: Notif) {
    return this.http.post(this.url, notification);
  }
}
