import { TestBed } from '@angular/core/testing';

import { RequeteHttpService } from './requete-http.service';

describe('RequeteHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequeteHttpService = TestBed.get(RequeteHttpService);
    expect(service).toBeTruthy();
  });
});
