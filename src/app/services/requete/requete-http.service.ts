import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Requete } from 'src/app/models/requete/requete';

@Injectable({
  providedIn: 'root'
})
export class RequeteHttpService {
  private url: string = 'http://localhost:8080/requetes';

  constructor(private http: HttpClient) { }

  getRequetes = () => {
    return this.http.get(this.url);
  }

  getRequetesByIdAgent = (id: number) => {
    return this.http.get(this.url + `/agent` + `/${id}`);
  }

  saveRequete(requete: Requete) {
    return this.http.post(this.url, requete);
  }

  getRequeteByIdRequete = (id: number) => {
    return this.http.get(this.url + `/${id}`);
  }
}
