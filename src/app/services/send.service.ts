import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class SendService {
    private subject = new Subject<any>();

    sendIdUtilisateur(idUtiService: any) {
        this.subject.next(idUtiService);
    }

    clearIdUtilisateur() {
        this.subject.next();
    }

    getIdUtilisateur(): Observable<any> {
        return this.subject.asObservable();
    }
}
