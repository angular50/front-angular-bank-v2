import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Transaction } from 'src/app/models/transaction/transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionHttpService {
  private url: string = 'http://localhost:8080/transactions';

  constructor(private http: HttpClient) { }

  getTransactions = () => {
    return this.http.get(this.url);
  }

  getTransactionsByIdCompte = (id: number) => {
    return this.http.get(this.url + `/compte` + `/${id}`);
  }

  getTransactionsByIdCompteMoisDernier = (id: number) => {
    return this.http.get(this.url + `/compte/derniers` + `/${id}`);
  }

  getTransactionsByIdCompteYear = (id: number) => {
    return this.http.get(this.url + `/compte/douzeMois` + `/${id}`);
  }

  getTransactionsByIdCompteMois = (id: number, month: number) => {
    return this.http.get(this.url + `/compte/mois` + `/${id}` + `/${month}`);
  }

  getTransactionsByIdCompteBetween = (id: number, dateDepart: Date, dateFin: Date) => {
    return this.http.get(this.url + `/compte` + `/${id}` + `/${dateDepart}` + `/${dateFin}`);
  }

  getSoldeByIdCompte = (id: number) => {
    return this.http.get(this.url + `/solde` + `/${id}`);
  }

  saveTransaction = (transaction: Transaction) => {
    return this.http.post(this.url, transaction);
  }

}
