import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TypeRequeteHttpService {
  private url: string = 'http://localhost:8080/typesrequetes';

  constructor(private http: HttpClient) { }

  getTypesRequetes = () => {
    return this.http.get(this.url);
  }
}
