import { TypeTransaction } from './../../models/typeTransaction/type-transaction';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TypeTransactionHttpService {
  private url: string = 'http://localhost:8080/typestransactions';

  constructor(private http: HttpClient) { }

  getTypesTransactions = () => {
    return this.http.get(this.url);
  }

  getTypeById = (id: number) => {
    return this.http.get(this.url + `/${id}`);
  }

  saveType = (typeTransaction: TypeTransaction) => {
    return this.http.post(this.url, typeTransaction);
  }
}
