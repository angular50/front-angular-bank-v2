import { TestBed } from '@angular/core/testing';

import { UtilisateurHttpService } from './utilisateur-http.service';

describe('UtilisateurHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UtilisateurHttpService = TestBed.get(UtilisateurHttpService);
    expect(service).toBeTruthy();
  });
});
