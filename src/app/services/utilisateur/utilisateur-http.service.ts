import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Utilisateur } from 'src/app/models/utilisateur/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurHttpService {
  private url: string = 'https://heroku-gestibankback.herokuapp.com/utilisateurs';

  constructor(private http: HttpClient) { }

  getUtilisateurs = () => {
    return this.http.get(this.url);
  }

  getUtilisateurById = (id: number) => {
    return this.http.get(this.url + `/${id}`);
  }

  saveUtilisateur = (utilisateur: Utilisateur) => {
    return this.http.post(this.url, utilisateur);
  }

  deleteUtilisateurById = (id: number) => {
    return this.http.delete(this.url + `/${id}`);
  }

  getUtilisateurByName = (nom: string) => {
    return this.http.get(this.url + `/nom` + `/${nom}`);
  }
}
