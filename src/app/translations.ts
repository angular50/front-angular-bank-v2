export const TRANSLATIONS = [


  {
    fr: 'FR',
    en: 'FR',
    ar: 'FR'
  },
  {
    fr: 'UK',
    en: 'UK',
    ar: 'UK'
  },
  {
    fr: 'عربى',
    en: 'عربى',
    ar: 'عربى'
  },
  // Accueil
  {
    fr: '50€ à l\'ouverture',
    en: '£50 cash bonus',
    ar: '50€ مجانا'
  },
  {
    fr: 'Nom',
    en: 'Name',
    ar: 'أسم'
  },
  {
    fr: 'Prenom',
    en: 'FirstName',
    ar: 'اِسْم مَسِيحِيّ'
  },
  {
    fr: 'adresse email',
    en: 'email address',
    ar: 'البريد الالكتروني'
  },
  {
    fr: 'Lancer la demande',
    en: 'Start request',
    ar: 'يرسل'
  },

  // Header

  {
    fr: 'Gestibanque',
    en: 'Gestibank',
    ar: 'إدارة البنك'
  },
  {
    fr: 'Actualites',
    en: 'News',
    ar:  'أخبار'
  },
  {
    fr: 'Demande Ouverture',
    en: 'Request Opening',
    ar: 'طلب فتح'
  },
  {
    fr: 'Mon espace Client',
    en: 'My Customer Area',
    ar: 'منطقة العملاء'
  },
  {
    fr: 'Convertir Devises',
    en: 'Convert Currency',
    ar: 'تحويل العملة'
  },
  {
    fr: 'Se connecter',
    en: 'Login',
    ar: 'دخول'
  },
  {
    fr: 'Deconnexion',
    en: 'Logout',
    ar: 'تسجيل خروج'
  },
  {
    fr: 'mot de passe',
    en: 'password',
    ar: 'كلمه السر'
  },

  // Footer
  {
    fr: 'Admin',
    en: 'Admin',
    ar: 'مشرف'
  },
  {
    fr: 'Agent',
    en: 'Advisor',
    ar: 'وكيل'
  },



]


